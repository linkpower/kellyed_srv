import sys

sys.path.append(".")
from os import path, environ, unlink
from datetime import datetime, date
from dateutil import parser as date_parser
from random import randint
from time import sleep, time
from hashlib import md5
import requests
import pytest


#http://www.classin-sdk.com/cn/login.html
#机构账号: 23605371301
#账号密码: 1as2sd

# classin_root = "api.eeo.cn"
# classin_root = "api.classinpaas.com"
classin_root = "api.classin-sdk.com"
# classin_api_url = "https://api.eeo.cn/partner/api/course.api.php?action={}"
# classin_api_url = "https://api.classin-sdk.com/partner/api/course.api.php?action={}"
classin_api_url = f"https://{classin_root}/partner/api/course.api.php?action={{}}"
# classin_api_url = f"https://httpbin.org/post"
classin_sid = 106684
classin_secret = "P0yrLUVi"

def classin_api(action: str, params: dict={}, files=None):
    timestamp = int(time())
    combined = classin_secret + str(timestamp)
    safe_key = md5(combined.encode()).hexdigest()
    # print("safe_key:", timestamp, combined, safe_key)
    params['SID'] = classin_sid
    params['safeKey'] = safe_key
    params['timeStamp'] = timestamp
    url = classin_api_url.format(action)
    print(f"url: {url}")
    print(f"params: {params}")
    res = requests.post(url, data=params, files=files)
    if res.status_code != 200:
        return f"{res.status_code}: {res.text}"
    return res.json() if res.text else res.text

def test_get_course_list():
    # data = classin_api('getCourseList')
    # data = classin_api('getFolderList')
    data = classin_api('getStudentList')
    print("getCourseList:", data)

def test_get_course():
    params = {'courseId': 42493}
    data = classin_api('getCourseInfo', params)
    print("getCourseInfo:", data)

def test_add_course():
    params = {
        'courseName': "API Test Course 4",
        'mainTeacherUid': 106684,
        'Filedata': "@~/classin.png" 
    }
    data = classin_api('addCourse', params)
    print("addCourse:", data)

def test_edit_course():
    img_file = "C:/Users/Jacky/Downloads/classin.png"
    fp = open(img_file, "rb")
    files = {'Filedata':('classin.png', fp)}
    params = {
        'courseId': 43229,
        'courseName': "API Test Course M4",
        'mainTeacherUid': 106684,
    }
    data = classin_api('editCourse', params, files=files)
    print("editCourse:", data)
    fp.close()

def test_upload_file():
    img_file = "C:/Users/Jacky/Downloads/classin.png"
    fp = open(img_file, "rb")
    files = {'Filedata':('classin.png', fp)}
    params = {
        'folderId': 8795,
    }
    data = classin_api('uploadFile', params, files=files)
    print("uploadFile:", data)
    fp.close()

def test_get_teacher():
    params = {
        'id': 1583
    }
    data = classin_api('getTeacher', params)
    print("getSchoolTeacherFullInfo:", data)


def test_add_label():
    params = {
        'labelName': 'Label 1'
    }
    data = classin_api('addSchoolLabel', params)
    print("addSchoolLabel:", data)

def test_get_label():
    params = {
        'labelId': 43
    }
    data = classin_api('getSchoolLabel', params)
    print("getSchoolLabel:", data)

def test_get_folder():
    data = classin_api('getFolderList')
    print("getFolderList:", data)

def teset_get_cloudlist():
    data = classin_api('getCloudList', {'folderId': 8795})
    print("getCloudList:", data)