import json
import sys

sys.path.append(".")
from os import path, environ, unlink
# set testing environment
environ['ENV'] = 'testing'
environ['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./test/out/sa_test.db'

from kellyed.core.config import settings
from kellyed.db.session import engine, SessionLocal
import sqlalchemy as sa
from sqlalchemy.orm import relationship, Session, column_property
from sqlalchemy.ext.declarative import declared_attr, declarative_base
import pyodbc
import pytest
import urllib

Base = declarative_base()

class Company(Base):
    __tablename__ = "company"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(50))

    employees = relationship(
        "Person", back_populates="company", cascade="all, delete-orphan"
    )

    def __repr__(self):
        return "Company %s" % self.name


class Person(Base):
    __tablename__ = "person"
    id = sa.Column(sa.Integer, primary_key=True)
    company_id = sa.Column(sa.ForeignKey("company.id"))
    name = sa.Column(sa.String(50))
    type = sa.Column(sa.String(50))

    company = relationship("Company", back_populates="employees")

    __mapper_args__ = {
        "polymorphic_identity": "person",
        "polymorphic_on": type,
    }

    def __repr__(self):
        return "Ordinary person %s" % self.name


class Engineer(Person):

    engineer_name = sa.Column(sa.String(30))
    primary_language = sa.Column(sa.String(30))

    # illustrate a single-inh "conflicting" column declaration;
    # see http://docs.sqlalchemy.org/en/latest/orm/extensions/
    #       declarative/inheritance.html#resolving-column-conflicts
    @declared_attr
    def status(cls):
        return Person.__table__.c.get("status", sa.Column(sa.String(30)))

    __mapper_args__ = {"polymorphic_identity": "engineer"}

    def __repr__(self):
        return (
            "Engineer %s, status %s, engineer_name %s, "
            "primary_language %s"
            % (
                self.name,
                self.status,
                self.engineer_name,
                self.primary_language,
            )
        )


class Manager(Person):
    manager_name = sa.Column(sa.String(30))

    @declared_attr
    def status(cls):
        return Person.__table__.c.get("status", sa.Column(sa.String(30)))

    __mapper_args__ = {"polymorphic_identity": "manager"}

    def __repr__(self):
        return "Manager %s, status %s, manager_name %s" % (
            self.name,
            self.status,
            self.manager_name,
        )

# Association Proxy
from sqlalchemy.ext.associationproxy import association_proxy

class UserKeyword(Base):
    __tablename__ = 'userkeywords'
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user2.id"), primary_key=True)
    keyword_id = sa.Column(sa.Integer, sa.ForeignKey("keyword.id"), primary_key=True)
    user = relationship("User")
    keyword = relationship("Keyword")

class User(Base):
    __tablename__ = 'user2'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(64))
    type = sa.Column(sa.String(50))
    kw = relationship("Keyword", secondary="userkeywords")

    __mapper_args__ = {
        'polymorphic_identity':'user',
        'polymorphic_on': type
    }

    def __init__(self, name):
        self.name = name

    # proxy the 'keyword' attribute from the 'kw' relationship
    # keywords = association_proxy('kw', 'keyword')
    keywords = association_proxy('kw', 'keyword',
                    creator=lambda kw: Keyword(keyword=kw))

    def __repr__(self):
        return "User id {}, status {}, keywords {}".format(
            self.id,
            self.name,
            self.keywords
        )

class Keyword(Base):
    __tablename__ = 'keyword'
    id = sa.Column(sa.Integer, primary_key=True)
    keyword = sa.Column('keyword', sa.String(64))

    def __init__(self, keyword):
        self.keyword = keyword

# userkeywords_table = sa.Table('userkeywords', Base.metadata,
#     sa.Column('user_id', sa.Integer, sa.ForeignKey("user2.id"),
#            primary_key=True),
#     sa.Column('keyword_id', sa.Integer, sa.ForeignKey("keyword.id"),
#            primary_key=True)
# )

class Staff(Base):
    __tablename__ = 'staff'
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(50))
    type = sa.Column(sa.String(50))

    __mapper_args__ = {
        'polymorphic_identity': 'staff',
        'polymorphic_on': type
    }

class Sales(Staff):
    __tablename__ = 'sales'

    sales_id = sa.Column(sa.Integer, sa.ForeignKey('staff.id'), primary_key=True)
    sales_name = sa.Column(sa.String(30))
    sales_mobile = sa.Column(sa.String(30))

    __mapper_args__ = {
        "polymorphic_identity": "sales",
    }

    def __repr__(self):
        return "Sales id {}, type {}, name {}, sales_id {}, sales_name {}, sales_mobile {}".format(
            self.id, self.type, self.name, self.sales_id, self.sales_name, self.sales_mobile)

class Teacher(Staff):
    __tablename__ = 'teacher'

    teacher_id = sa.Column(sa.Integer, sa.ForeignKey('staff.id'), primary_key=True)
    teacher_name = sa.Column(sa.String(30))
    teacher_mobile = sa.Column(sa.String(30))

    __mapper_args__ = {
        "polymorphic_identity": "teacher",
    }

    def __repr__(self):
        return "Sales id {}, type {}, name {}, teacher_id {}, teacher_name {}, teacher_mobile {}".format(
            self.id, self.type, self.name, self.teacher_id, self.teacher_name, self.teacher_mobile)

db: Session = None

def setup_module():
    # setup for pytest
    global db
    db = SessionLocal()

def shutdown_module():
    db.close()

def test_create_db():
    clear_all = True
    db_file = "./test/out/sa_test.db"
    if clear_all and path.exists(db_file):
        unlink(db_file)
    Base.metadata.create_all(bind=engine)

def test_polymorphic_multi_table():
    # staff = Staff(name='Peter')
    # db.add(staff)
    # db.commit()
    # sales = Sales(id=8,sales_id=8, sales_name='jek', sales_mobile='1234')
    sales = Sales(name='Peter', sales_name='jek', sales_mobile='1234')
    db.add(sales)
    db.commit()
    for u in db.query(Sales).all():
        # if u.id == 1:
        #     db.delete(u)
        #     db.commit()
        if u.id == 2:
            u.name += '2'
            u.sales_name += '2'
            db.add(u)
            db.commit()
        print("Sales: {}".format(u))

def test_polymorphic_one_table():
    c = Company(
        name="company1",
        employees=[
            Manager(
                name="pointy haired boss", status="AAB", manager_name="manager1"
            ),
            Engineer(
                name="dilbert",
                status="BBA",
                engineer_name="engineer1",
                primary_language="java",
            ),
            Person(name="joesmith"),
            Engineer(
                name="wally",
                status="CGG",
                engineer_name="engineer2",
                primary_language="python",
            ),
            Manager(name="jsmith", status="ABA", manager_name="manager2"),
        ],
    )
    db.add(c)
    db.commit()

    company_total = db.query(Company).count()
    person_total = db.query(Person).count()
    print(f"total: {company_total}, {person_total}")


def test_polymorphic_join():
    pass

def test_user_keywords():
    user = User('jek')
    user.keywords = ['pear', 'orange']
    user.keywords.append('cheese')
    user.keywords.append('apple')
    db.add(user)
    db.commit()
    for u in db.query(User).all():
        print("User: {}".format(u))

def test_mssql_connect():
    # conn_str = "Driver={SQL Server Native Client 11.0};Server=(LocalDB)\\MSSQLLocalDB;Integrated Security=true;AttachDbFileName=C:\\Project\\lp\\KellyEd\\kellyed_srv\\instance\\kellyed.mdf"
    conn_str = "Driver={SQL Server Native Client 11.0};Server=(LocalDB)\\MSSQLLocalDB;Database=kellyed;Integrated Security=true"
    sql = """SELECT TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='dbName'"""
    conn = pyodbc.connect(conn_str)
    print("quoted connection string:", urllib.parse.quote(conn_str))
    print("execute SQL")
    cursor = conn.cursor()
    cursor.execute(sql)
    for row in cursor:
        print('---', row)
    conn.close()