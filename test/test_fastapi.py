import sys

from starlette.testclient import Params

sys.path.append(".")
from os import path, environ, unlink
# set testing environment
environ['ENV'] = 'testing'
environ['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///./instance/kellyed_test.db'
environ['SQLALCHEMY_ECHO'] = 'False'

from fastapi.testclient import TestClient
from kellyed.main import app, app_init
from kellyed.core.config import settings
from kellyed.db import models, schemas, session, crud
from kellyed.core import utils, excel
from datetime import datetime, date
from dateutil import parser as date_parser
from random import randint
from time import sleep
import pytest

client = TestClient(app)

def setup_module():
    # setup for pytest
    app_init(logger_config=utils.load_json("./cfg/log_testing.json"))

def test_db_orm():
    db = session.SessionLocal()
    choices = db.query(models.Choice).all()
    for choice in choices:
        print("choice:", choice.text, choice.value)
    if False:
        student = models.Student(
            student_id=1,
            user_id=1,
            student_code='STC1',
            trial_student_code='TSOW1',
            referral_code='RC1',
            referral_name='Jacky',
            name='Peter Chan',
            parent_1='David Chan',
            parent_2='May Lee',
            mobile_whatsapp='12345678',
            mobile_classin='91234567',
            dob=date(2010, 10, 14),
            gender='Male',
            created_at = datetime.now(),
            updated_at = datetime.now()
        )
        db.add(student)
        db.commit()

def test_db_from_orm():
    db_user = models.User(
        user_id=1,
        name="Admin",
        password="#hashed_password",
        initial="AD",
        email="admin@a.com",
        mobile="",
        disabled=False,
        roles=['Admin', 'Student']
    )
    s_user = schemas.UserBrief.from_orm(db_user)
    print("s_user:", s_user.dict())

def test_hash_encrypt():
    text = "admin"
    htext = utils.hash_b64(text)
    print(f"hash_b64: {text} -> {htext}")
    text = 'postgresql+psycopg2://kadmin:kelly2021@localhost/kellyed'
    enc_str = utils.encrypt(text)
    print(f"encrypt: {text} -> {enc_str}")
    dec_str = utils.decrypt(enc_str)
    print(f"decrypt: {enc_str} -> {dec_str}")

def test_auth_login():
    # verify access by getting current user
    res = client.get('/api/auth/current_user')
    assert res.status_code == 401, res.text
    print('auth verify: Failed,', res.text)
    # login, password: admin
    data = {
        "username": "admin@a.com",
        "password": "jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=",
        # "grant_type": "password"
    }
    res = client.post('/api/auth/login', data=data)
    assert res.status_code == 200, res.text
    token = res.json()
    print('login token:', token)
    # sleep(2)
    # verify access again with authorization header
    res = client.get('/api/auth/current_user', headers={
        "Authorization": token['token_type'] + ' ' + token['access_token']
    })
    assert res.status_code == 200, res.text
    print('auth verify: OK,', res.json())
    # sleep 1s for new token before refresh
    sleep(1)
    # use refresh token to get new access token
    res = client.get('/api/auth/refresh', headers={
        "Authorization": token['token_type'] + ' ' + token['refresh_token']
    })
    assert res.status_code == 200, res.text
    new_token = res.json()
    print('refresh new token:', new_token)
    # use new access token
    res = client.get('/api/auth/current_user', headers={
        "Authorization": token['token_type'] + ' ' + new_token['access_token']
    })
    assert res.status_code == 200, res.text
    print('auth verify: OK,', res.json())


def test_create_db():
    clear_all = True
    import_sql = False
    db_file = "./instance/kellyed_test.db"
    if clear_all and path.exists(db_file):
        unlink(db_file)
    db = session.SessionLocal()
    # models.db_drop_all()
    models.db_create_all()
    if import_sql:
        with open("./sql/sample1_sqlite.sql", encoding='utf8') as f:
            lines = f.readlines()
            sql = ''
            for line in lines:
                line = line.strip()
                if line == '' or line.startswith('#'):
                    continue
                sql += line
                if line.endswith(';'):
                    # execute raw sql
                    db.get_bind().execute(sql)
                    sql = ''
            db.commit()
    total = db.query(models.Student).count()
    print(f"Student total: {total}")
    if import_sql:
        assert total > 0

def test_status():
    res = client.get("/api/status")
    assert res.status_code == 200
    assert res.text == 'OK'

def test_static_index():
    res = client.get('/static/', allow_redirects=True)
    assert res.status_code == 200
    body = res.text
    print("body:", body)
    assert body.find("kellyedui") != -1
    res = client.get('/', allow_redirects=True)
    assert res.status_code == 200
    body = res.text
    print("body:", body)
    assert body.find("kellyedui") != -1

def test_status():
    res = client.get("/api/status")
    assert res.status_code == 200
    assert res.text == 'OK'

@pytest.mark.parametrize("input", [
    'staffs', 'students', 'teachers', 'choices', 'products',
    'courses', 'lessons', 'salesorders', 'payments'])
def test_list_json(input: str):
    res = client.get(f"/api/{input}?page_size=1000")
    assert res.status_code == 200, res.text
    for r in res.json():
        print(f"{input}:", r)
    # print json format
    print(res.text)

@pytest.mark.parametrize("input", [
    'staffs', 'students', 'teachers', 'choices', 'products',
    'courses', 'lessons', 'salesorders', 'payments'])
def test_list_count(input: str):
    res = client.get(f"/api/{input}/count")
    assert res.status_code == 200, res.text
    data = res.json()
    print(f"{input}:", data['count'])

def test_crud_staff():
    db = session.SessionLocal()
    qstaff = crud.read_staff(db, 1)
    print("admin:", qstaff.name, qstaff.roles)
    staff = models.Staff(
        name='Jacky',
        initial='JA',
        # hpasswd='jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=',
        password='admin',
        email="jacky@a.com",
        roles=['Admin', 'Sales']
    )
    db.add(staff)
    db.commit()
    print("staff:", staff.name, staff.roles)

def test_crud_students():
    db = session.SessionLocal()
    student = crud.read_student(db, 2)
    print("student:", student.name, student.referral_code, student.__dict__)
    student.name = 'Name ' + str(randint(1000, 1999))
    student.student_code = 'ST' + str(randint(1000, 1999))
    student.referral_code = 'RC' + str(randint(1000, 1999))
    db.add(student)
    db.commit()
    student = crud.read_student(db, 2)
    print("student:", student.name, student.student_code, student.referral_code, student.__dict__)

def test_crud_teachers():
    is_update = True
    db = session.SessionLocal()
    teacher = crud.read_teacher(db, 9)
    print("teacher:", teacher.name, teacher.__dict__)
    # for t in teacher.product_ids:
    #     # print("tp:", t.user_id, t.product_id)
    #     print("tp:", t)
    for t in teacher.products:
        print("product:", t.product_id, t.name)
    if is_update:
        # teacher.product_ids.set(models.TeacherProduct(product_id=5))
        # teacher.product_ids = {x: models.TeacherProduct(product_id=x) for x in [1,2,3,4]}
        teacher.products = [3,6,2,4,5]
        db.add(teacher)
        db.commit()
        print("After Updated:")
        for t in teacher.products:
            print("product:", t.product_id, t.name)
    # s_teacher = schemas.TeacherDetail.from_orm(teacher)
    # for t in s_teacher.products:
    #     print("product:", t)

def test_crud_course():
    db = session.SessionLocal()
    course = crud.read_course(db, 1)
    print("course:", course.name, course.__dict__)

def test_staff_list():
    search_options = {
        'keyword': '',
        'page': 0,
        'page_size': 100,
        'sort_by': '',
        'sort_desc': False,
        'names_only': True,
        'roles': 'Teacher,Admin'
    }
    res = client.get(f"/api/staffs", params=search_options)
    assert res.status_code == 200, res.text
    for staff in res.json():
        print("Staff:", staff)

@pytest.mark.parametrize("input", [1])
def test_staff_get(input: int):
    res = client.get(f"/api/staffs/{input}")
    assert res.status_code == 200
    print("staff:", res.json())

def test_student_list():
    search_options = {
        'keyword': '',
        'page': 0,
        'page_size': 10,
        'sort_by': 'student_code',
        'sort_desc': True
    }
    res = client.get(f"/api/students/count", params={
        'keyword': search_options['keyword']
    })
    assert res.status_code == 200
    data = res.json()
    print("student count:", data["count"])
    res = client.get(f"/api/students", params=search_options)
    assert res.status_code == 200
    data = res.json()
    for student in data:
        print("student:", student)
    # get student by ids
    res = client.get(f"/api/students", params={'ids': "6,8"})
    data = res.json()
    print("get student by ids:")
    for student in data:
        print("student:", student)


@pytest.mark.parametrize("input", [6, 7])
def test_student_get(input: int):
    res = client.get(f"/api/students/{input}")
    assert res.status_code == 200
    print("student:", res.json())

def test_student_create():
    data = {
        'name': 'Peter Chan',
        'initial': 'ST',
        'password': 'admin',
        'email': "jacky@a.com",
        'mobile': "12345678",
        'roles': ['Student'],
        'student_code': 'STC3',
        'trial_student_code': 'TSOW3',
        'referral_code': 'RC3',
        'referral_name': 'Sam',
        'parent_1': 'David Chan',
        'parent_2': 'May Lee',
        'mobile_whatsapp': '12345678',
        'mobile_classin': '91234567',
        'dob': '2010-10-14',
        'gender': 'Male',
    }
    res = client.post('/api/students', json=data)
    assert res.status_code == 200
    print("Create student:", res.json())

def test_student_update(input=6):
    cur_time = datetime.now()
    # generate new value
    new_value = 'STS' + str(randint(1000, 1999))
    # get student
    res = client.get(f'/api/students/{input}')
    data = res.json()
    data['referral_code'] = new_value
    # update student
    res = client.put(f'/api/students/{input}', json=data)
    assert res.status_code == 202, res.text
    print(f"Update student 1: value {new_value}")
    # get student to check update
    res = client.get(f'/api/students/{input}')
    data = res.json()
    mtime = date_parser.parse(data['updated_at'], ignoretz=True)
    print(f"Get student 1: {data}")
    assert data['referral_code'] == new_value
    assert mtime > cur_time

def test_student_delete():
    res = client.delete('/api/students/12')
    assert res.status_code == 202, res.text

@pytest.mark.parametrize('input', ['role','gender'])
def test_list_choices(input: str):
    res = client.get(f"/api/choices/{input}")
    assert res.status_code == 200, res.text
    data = res.json()
    for choice in data:
        print("Choice '{}' {}: {}".format(input, choice['text'], choice['value']))

def test_excel_xlsx():
    class Struct:
        def __init__(self, **entries):
            self.__dict__.update(entries)
    replace = True
    src_xlsx = 'test1.xlsx'
    out_xlsx = 'test1_out.xlsx'
    src_path = path.join('./test/out', src_xlsx)
    out_path = path.join('./test/out', out_xlsx)
    choices = [
        Struct(**{ 'choice_id': 1, 'category': 'gender', 'text': 'Male', 'value': 'Male', 'index': 0 }),
        Struct(**{ 'choice_id': 2, 'category': 'gender', 'text': 'Female', 'value': 'Female', 'index': 1 }),
    ]
    if replace:
        xlwb = excel.open_xlsx(src_path)
    else:
        xlwb = excel.create_xlsx(['Choice'])
    excel.update_sheet(xlwb, 'Choice', choices)
    excel.save_xlsx(xlwb, out_path)
    records, id = excel.get_sheet_items(xlwb, 'Choice')
    for i, r in enumerate(records, start=1):
        print(f"record {i}: {r}")

def test_import_xlsx():
    sample_sheets = settings.sheets
    # sample_sheets = ['Staff']
    #sample_sheets = ['Student']
    sample_xlsx = "./samples/import1.xlsx"
    upload_file = open(sample_xlsx, 'rb')
    data = {'sheets': sample_sheets, 'truncate': True}
    files = {'file': (path.basename(sample_xlsx), upload_file)}
    res = client.post("/api/admin/import", data=data, files=files)
    assert res.status_code == 200, res.text
    data = res.json()
    print("Imported: {}\n{}".format(data['imported'], data['messages']))
    upload_file.close()
    # check imported
    search_options = {'page_size': 0}   # unlimited records
    for sheet in sample_sheets:
        res = client.get('/api/{}s'.format(sheet.lower()), json=search_options)
        if res.status_code != 200:
            print(f"List '{sheet}' Failed:", res.text)
        else:
            data = res.json()
            if len(data) == 0:
                print(f"List '{sheet}': not records")
            for item in data:
                print(f"Imported '{sheet}':", item)

def test_export_xlsx():
    have_src = True
    src_xlsx = 'import1.xlsx'
    src_path = path.join('./samples', src_xlsx)
    out_xlsx = 'export1.xlsx'
    out_path = path.join('./test/out', out_xlsx)
    out_sheets = ['Staff', 'Student', 'Teacher', 'Choice', 'Product']
    upload_file = open(src_path, 'rb') if have_src else None
    data = {
        'filename': out_xlsx, 
        'sheets': out_sheets
    }
    files = {'file': (src_xlsx, upload_file)} if upload_file else None
    res = client.post("/api/admin/export", data=data, files=files)
    assert res.status_code == 200, res.text
    if upload_file:
        upload_file.close()
    with open(out_path, 'wb') as fp:
        fp.write(res.content)
    print("exported xlsx to:", out_path)

@pytest.mark.parametrize("input", ['Interest Group', 'Maths', 'Drama'])
def test_list_product_category(input):
    res = client.get(f"/api/products/category/{input}")
    assert res.status_code == 200, res.text
    for product in res.json():
        print("product:", product)


@pytest.mark.parametrize("input", [9, 10])
def test_teacher_get(input: int):
    res = client.get(f"/api/teachers/{input}")
    assert res.status_code == 200
    print("teacher:", res.json())

@pytest.mark.parametrize("input", [9, 10])
def test_teacher_update(input):
    # generate new value
    new_value = 'Teacher ' + str(randint(1000, 1999))
    # get student
    res = client.get(f"/api/teachers/{input}")
    data = res.json()
    data['name'] = new_value
    # data['roles'] = ['Teacher', 'Teacher Advisor']
    data['products'] = [3,4,5]
    # update teacher
    res = client.put(f"/api/teachers/{input}", json=data)
    assert res.status_code == 202, res.text
    print(f"Update teacher {input}: value {new_value}")
    # get teacher to check update
    res = client.get(f"/api/teachers/{input}")
    data = res.json()
    print(f"Get teacher {input}: {data}")


@pytest.mark.parametrize("input", [1, 2])
def test_course_get(input: int):
    res = client.get(f"/api/courses/{input}")
    assert res.status_code == 200
    course = res.json()
    print("course:", course)
    print("lessons:", course['lessons'])


def test_course_list():
    search_options = {
        'keyword': '',
        'page': 0,
        'page_size': 100,
        'sort_by': 'total_classes',
        'sort_desc': False
    }
    res = client.get(f"/api/courses", params=search_options)
    assert res.status_code == 200, res.text
    for course in res.json():
        print("Course:", course)

def test_lesson_list():
    search_options = {
        'keyword': '',
        'page': 0,
        'page_size': 100,
        # 'sort_by': 'schedule_time',
        'sort_by': 'course_name',
        'sort_desc': False
    }
    res = client.get(f"/api/lessons", params=search_options)
    assert res.status_code == 200, res.text
    for lesson in res.json():
        print("Lesson:", lesson)

@pytest.mark.parametrize("input", [1, 4])
def test_lesson_get(input: int):
    res = client.get(f"/api/lessons/{input}")
    assert res.status_code == 200
    print("lesson:", res.json())

def test_lesson_update():
    # get student
    input = 1
    res = client.get(f"/api/lessons/{input}")
    data = res.json()
    data['student_actions'] = [
        { 'user_id': 6, 'attendance': 'Present' },
        { 'user_id': 7, 'attendance': 'Skipped' },
        { 'user_id': 8, 'attendance': 'Present' }
    ]
    # update lesson
    res = client.put(f"/api/lessons/{input}", json=data)
    assert res.status_code == 202, res.text
    print(f"Update lesson {input}")
    # get lesson to check update
    res = client.get(f"/api/lessons/{input}")
    data = res.json()
    print(f"Get lesson {input}: {data}")

def test_salesorder_list():
    search_options = {
        'keyword': '',
        'page': 0,
        'page_size': 100,
        'sort_by': '',
        'sort_desc': False
    }
    res = client.get(f"/api/salesorders", params=search_options)
    assert res.status_code == 200, res.text
    for salesorder in res.json():
        print("SalesOrder:", salesorder)

def test_salesorder_get(input: int=1):
    res = client.get(f"/api/salesorders/{input}")
    assert res.status_code == 200
    print("salesorder:", res.json())
