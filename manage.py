# SQLAlchemy migration manager

import sys
sys.path.append(".")

import os
from kellyed.core.config import settings
from kellyed.db import session, models
from kellyed.core import excel, utils
from alembic.config import Config
from alembic import command
import argparse
# import warnings
import logging
import logging.config

logger = logging.getLogger(__name__)

# pgsql_del_sql = """
# DROP SCHEMA public CASCADE;
# CREATE SCHEMA public;
     
# GRANT ALL ON SCHEMA public TO postgres;
# GRANT ALL ON SCHEMA public TO public;
# """

# mssql_del_sql = 'EXEC sp_MSforeachtable @command1 = "  DROP TABLE ?"'


# ensure the instance folder exists
project_path = os.path.dirname(os.path.realpath(__file__))
try:
    os.chdir(project_path)
    os.makedirs(settings.instance_path)
except OSError:
    pass

logger_config=utils.load_json("./cfg/log_testing.json")
logging.config.dictConfig(logger_config)
alembic_cfg = Config("./alembic.ini")

# warnings.simplefilter("ignore")

# include object filter
# - skip table 'sqlite_sequence'
def include_object(object, name, type_, reflected, compare_to):
    if (type_ == "table" and name == 'sqlite_sequence' or
        not reflected and object.info.get("skip_autogenerate", False)):
        return False
    else:
        return True

def add_sample():
    "add samples data to database"

    print(f"Import from: {settings.sample_xlsx}")
    db = session.SessionLocal()
    imported, mesassges = excel.import_excel(db, 
        src_xslx=settings.sample_xlsx, sheets=settings.sheets, truncate=False)
    print(f"Total Imported: {imported}")
    print(f"{mesassges}")
    # sql_file = "./sql/sample1.sql"
    # with open(sql_file, encoding='utf8') as f:
    #     lines = f.readlines()
    #     sql = ''
    #     for line in lines:
    #         line = line.strip()
    #         if line == '' or line.startswith('#'):
    #             continue
    #         sql += line
    #         if line.endswith(';'):
    #             # execute raw sql
    #             db.get_bind().execute(sql)
    #             sql = ''
    #     db.commit()
    print(f"Samples added")

def setup(drop_all=False):
    "server init setup"

    os.chdir(project_path)
    # create necessary paths
    if not os.path.exists(settings.instance_path):
        os.makedirs(settings.instance_path)
    if not os.path.exists(settings.log_folder):
        os.makedirs(settings.log_folder)

    # delete database file
    if drop_all:
        models.db_drop_all()
        print(f"Dropped all tables")
        # if settings.sqlalchemy_database_uri.startswith("sqlite:"):
        #     db_file = settings.sqlalchemy_database_uri.replace("sqlite:///", "")
        #     if os.path.exists(db_file):
        #         print(f"Delete db file: {db_file}")
        #         os.unlink(db_file)
        # elif settings.sqlalchemy_database_uri.startswith("postgresql"):
        #     db = session.SessionLocal()
        #     db.get_bind().execute(pgsql_del_sql)
        # elif settings.sqlalchemy_database_uri.startswith("mssql"):
        #     db = session.SessionLocal()
        #     db.get_bind().execute(mssql_del_sql)

    # create database by alembic upgrade to head
    db_upgrade()
    models.db_create_all()

    # copy server production config from template
    print(f"Setup completed")

def create_superuser():
    data = {
        'email': 'admin@a.com',
        'password': 'admin',
        'name': 'Admin',
        'initial': 'AD',
        'staff_code': 'AD1',
        'mobile': '',
        'roles': ['Admin']
    }
    db = session.SessionLocal()
    admin = db.query(models.Staff).filter_by(email='admin@a.com').one_or_none()
    # don't create if have
    if admin:
        return
    # if not admin:
    #     admin = models.Staff(**data)
    # else:
    #     for key, value in data.items():
    #         setattr(admin, key, value)
    admin = models.Staff(**data)
    db.add(admin)
    db.commit()
    print(f"Created Superuser: {data['email']}")

def db_revision(rev_id: str):
    "create db revision"
    command.revision(alembic_cfg, rev_id=rev_id, autogenerate=True)
    
def db_upgrade():
    "server db upgrade"
    command.upgrade(alembic_cfg, "head")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--drop_all", help="drop all tables in KellyEd database", 
        action="store_true", default=False)
    parser.add_argument("-s", "--setup", help="setup KellyEd and database", 
        action="store_true", default=False)
    parser.add_argument("-c", "--createsuperuser", help="create super user", 
        action="store_true", default=False)
    parser.add_argument("-a", "--add_sample", help="add data samples", 
        action="store_true", default=False)
    parser.add_argument("-u", "--upgrade", help="server db upgrade", 
        action="store_true", default=False)
    parser.add_argument("-r", "--revision", help="server db revision with id", 
        metavar="rev_id", default=None)
    args = parser.parse_args()
    run_cmd = False
    if args.revision:
        db_revision(args.revision)
        run_cmd = True
    if args.setup:
        setup(args.drop_all)
        run_cmd = True
    elif args.upgrade:
        db_upgrade()
        run_cmd = True
    if args.createsuperuser:
        create_superuser()
        run_cmd = True
    if args.add_sample:
        add_sample()
        run_cmd = True
    if not run_cmd:
        parser.print_help()