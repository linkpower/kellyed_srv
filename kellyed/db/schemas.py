from datetime import datetime, date
from pydantic import BaseModel, Json, validator, Field
from typing import Dict, List, Optional, Any, Union
from sqlalchemy.ext.associationproxy import _AssociationList

# search options for page list
class SearchOptions(BaseModel):
    keyword: str = Field(None)
    page: int = Field(0)
    page_size: int = Field(10)
    sort_by: str = Field(None)
    sort_desc: bool = Field(False)

# Access Token Schema
class AccessToken(BaseModel):
    token_type: str
    access_token: str
    refresh_token: str
    expire_time: int

# convert assocation list to list for validator
def assoc_list_validate(item_schema):
    def validate(cls, value):
        if not isinstance(value, list) and not isinstance(value, _AssociationList):
            raise TypeError("List or AssociationList required")
        return [item_schema.from_orm(u) for u in value]
    return validate

# Choice Schema
class Choice(BaseModel):
    choice_id: int
    category: str
    text: str
    value: Any

    class Config:
        orm_mode = True

class ChoiceCreate(BaseModel):
    category: str
    text: str
    value: Any

    class Config:
        orm_mode = True

# User Schema
class UserRole(BaseModel):
    role: str

    class Config:
        orm_mode = True

class UserBase(BaseModel):
    name: str
    initial: str
    email: str
    mobile: str
    disabled: bool = False
    roles: Optional[List[Union[str, UserRole]]]

    @validator('roles', pre=True)
    def set_roles(cls, v):
        # switch between string and UserRole
        return [UserRole(role=x) if type(x) == str else x.role for x in v]

class User(UserBase):
    user_id: int

    class Config:
        orm_mode = True

class UserCreate(UserBase):
    password: str

    class Config:
        orm_mode = True

class UserUpdate(UserBase):
    user_id: int
    password: Optional[str]

    class Config:
        orm_mode = True


# Staff Schema
class StaffBase(UserBase):
    staff_code: str
    gender: str

class Staff(StaffBase):
    user_id: int

    class Config:
        orm_mode = True

class StaffCreate(StaffBase):
    password: str

    class Config:
        orm_mode = True

class StaffUpdate(StaffBase):
    user_id: int
    password: Optional[str]

    class Config:
        orm_mode = True


# Student Schema
class StudentBase(UserBase):
    student_code: str
    trial_student_code: str
    referral_code: str
    referral_name: str
    parent_1: str
    parent_2: str
    mobile_whatsapp: str
    mobile_classin: str
    dob: date
    gender: str

class Student(StudentBase):
    user_id: int

    class Config:
        orm_mode = True

class StudentDetail(StudentBase):
    user_id: int
    updated_at: datetime

    class Config:
        orm_mode = True

class StudentCreate(StudentBase):
    password: str

    class Config:
        orm_mode = True

class StudentUpdate(StudentBase):
    user_id: int
    password: Optional[str]

    class Config:
        orm_mode = True

# Product Schema
class ProductBase(BaseModel):
    product_code: str
    name: str
    trial: bool
    category: str
    language: str
    formation: str
    level: Optional[str]
    lesson_price: float

class ProductBrief(BaseModel):
    product_id: int
    product_code: str
    name: str
    # label: Optional[str]

    class Config:
        orm_mode = True

    # @validator('label', always=True)
    # def set_label(cls, v, values):
    #     return values['product_code'] + ': ' + values['name']

class Product(ProductBase):
    product_id: int

    class Config:
        orm_mode = True

class ProductCreate(ProductBase):

    class Config:
        orm_mode = True

class ProductUpdate(ProductBase):
    product_id: int

    class Config:
        orm_mode = True

# Teacher Schema
class TeacherBase(UserBase):
    teacher_code: str
    mobile_whatsapp: str
    mobile_classin: str
    gender: str
    hourly_rate: float
    trial_bonus: float

class Teacher(TeacherBase):
    user_id: int

    class Config:
        orm_mode = True

class TeacherDetail(TeacherBase):
    user_id: int
    updated_at: datetime
    products: List[ProductBrief]
    # convert assocation list to list
    _products = validator('products', pre=True, allow_reuse=True)(assoc_list_validate(ProductBrief))

    class Config:
        orm_mode = True

    # @validator('products', pre=True)
    # def validate(cls, v):
    #     if not isinstance(v, list) and not isinstance(v, _AssociationList):
    #         raise TypeError("List or AssociationList required")
    #     return [Product.from_orm(u) for u in v]

class TeacherCreate(TeacherBase):
    password: str
    products: Optional[List[int]]

    class Config:
        orm_mode = True

class TeacherUpdate(TeacherBase):
    user_id: int
    password: Optional[str]
    products: Optional[List[int]]

    class Config:
        orm_mode = True

class LessonBase(BaseModel):
    course_id: int
    teacher_id: Optional[int]
    ta_id: Optional[int]
    is_free: bool
    lesson_number: int
    status: str
    schedule_start: Optional[datetime]
    schedule_end: Optional[datetime]
    start_time: Optional[datetime]
    end_time: Optional[datetime]

# Course Schema
class CourseBase(BaseModel):
    product_id: int
    teacher_id: Optional[int]
    course_code: str
    name: str
    status: str
    paid_classes: int
    free_classes: int
    free_group_classes: int
    total_classes: Optional[int]
    lesson_first_date: Optional[date]
    end_date: Optional[date]

class Course(CourseBase):
    course_id: int

    class Config:
        orm_mode = True

class LessonBrief(LessonBase):
    lesson_id: int

    class Config:
        orm_mode = True

class CourseDetail(CourseBase):
    course_id: int
    course_intro: Optional[str]
    updated_at: datetime
    product: Product
    teacher: Optional[Teacher]
    students: List[Student]
    lessons: List[LessonBrief]
    # convert assocation list to list
    _students = validator('students', pre=True, allow_reuse=True)(assoc_list_validate(Student))

    class Config:
        orm_mode = True

class CourseCreate(CourseBase):
    students: Optional[List[int]]   # student ids

    class Config:
        orm_mode = True

class CourseUpdate(CourseBase):
    course_id: int
    students: Optional[List[int]]   # student ids

    class Config:
        orm_mode = True

# Lessons Schema
class Lesson(LessonBase):
    lesson_id: int
    updated_at: datetime
    course_name: str
    course_code: str
    total_classes: str

    class Config:
        orm_mode = True

    # set lesson name by using course name, lesson number, total_classes
    # @validator('name', always=True)
    # def set_course_name(cls, v, values):
    #     course = values.get('course')
    #     if course:
    #         v = "{} #{}/{}".format(course['name'], values['lesson_number'], course['total_classes'])
    #     return v

class StudentLesson(BaseModel):
    user_id: int
    lesson_id: Optional[int]
    attendance: str

    class Config:
        orm_mode = True


class LessonDetail(LessonBase):
    lesson_id: int
    updated_at: datetime
    course: Course
    teacher: Optional[Teacher]
    ta: Optional[Staff]
    students: List[Student]
    student_actions: List[StudentLesson]
    # convert assocation list to list
    _students = validator('students', pre=True, allow_reuse=True)(assoc_list_validate(Student))

    class Config:
        orm_mode = True

class LessonCreate(LessonBase):
    student_actions: Optional[List[StudentLesson]]

    class Config:
        orm_mode = True

class LessonUpdate(LessonBase):
    lesson_id: int
    student_actions: Optional[List[StudentLesson]]

    class Config:
        orm_mode = True

# SalesOrders and Payment Schema
class SalesOrderBase(BaseModel):
    so_id: int
    sales_id: int
    course_id: int
    student_id: int
    so_number: str
    so_date: Optional[date]
    status: str
    existing: bool
    promo_code: Optional[str]
    currency: Optional[str]
    so_amount: float
    ex_rate: float
    so_amount: float
    so_gross: float
    so_discount: float
    so_net: float
    # course_value: float
    so_issued: bool

class SalesOrder(SalesOrderBase):
    so_id: int
    sales_name: str
    course_name: str
    student_name: str

    class Config:
        orm_mode = True

class PaymentBase(BaseModel):
    so_id: int
    received_date: Optional[date]
    received_amount: Optional[float]
    payee_name: Optional[str]
    payee_ref: Optional[str]
    bank_account: Optional[str]
    bank_ref: Optional[str]
    received_by: Optional[int]
    receipt_no: Optional[str]
    receipt_date: Optional[date]
    # [additional]
    account_code: Optional[str]
    signature_approval: Optional[str]
    payment_method: Optional[str]
    last_contact_date: Optional[date]
    attrition_code: Optional[str]
    remarks: Optional[str]

class Payment(PaymentBase):
    payment_id: int
    so_number: str
    so_date: date
    so_amount: str
    so_status: str
    receiver_name: str

    class Config:
        orm_mode = True

class SalesOrderDetail(SalesOrderBase):
    so_id: int
    updated_at: datetime
    sales: Optional[Staff]
    course: Optional[Course]
    student: Optional[Student]
    payment: Optional[Payment]

    class Config:
        orm_mode = True

class SalesOrderCreate(SalesOrderBase):

    class Config:
        orm_mode = True

class SalesOrderUpdate(SalesOrderBase):
    so_id: int

    class Config:
        orm_mode = True

class PaymentDetail(PaymentBase):
    payment_id: int
    updated_at: datetime
    sales_order: Optional[SalesOrder]
    receiver: Optional[Staff]
    student: Optional[Student]

    class Config:
        orm_mode = True

class PaymentCreate(PaymentBase):

    class Config:
        orm_mode = True

class PaymentUpdate(PaymentBase):
    payment_id: int

    class Config:
        orm_mode = True
