import sqlalchemy as sa
from sqlalchemy.orm import relationship, column_property, aliased
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.sql.expression import or_, and_, not_, asc, desc
from datetime import datetime
from .session import Base, engine, is_sqlite, is_postgre

# custom type for JSON except for sqlite and postgre SQL
import json
from sqlalchemy import TypeDecorator, types

class CustomJson(TypeDecorator):

    @property
    def python_type(self):
        return object

    impl = types.String

    def process_bind_param(self, value, dialect):
        return json.dumps(value)

    def process_literal_param(self, value, dialect):
        return value

    def process_result_value(self, value, dialect):
        try:
            return json.loads(value)
        except (ValueError, TypeError):
            return None

Json = sa.JSON if is_sqlite or is_postgre else CustomJson 

def db_create_all():
    Base.metadata.create_all(bind=engine)

def db_drop_all():
    Base.metadata.drop_all(bind=engine)

def db_recreate(model):
    model.__table__.drop(bind=engine)
    model.__table__.create(bind=engine)

def db_truncate(session, model):
    # get table name from Model or Table
    table_name = model.__tablename__
    if engine.driver == 'pysqlite':
        # truncate table by DELETE FROM
        session.execute("DELETE FROM sqlite_sequence WHERE name='{}'".format(table_name))
        # table_args = getattr(model, '__table_args__', getattr(model, 'kwargs', None))
        table_args = getattr(model, '__table_args__', None)
        # reset autoincrement in sqlite_autoincrement if have
        if table_args and table_args.get('sqlite_autoincrement', False):
            session.execute(f'DELETE FROM "{table_name}"')
    else:
        # use SQL TRUNCATE for other databases
        session.execute(f'TRUNCATE "{table_name}" CASCADE')


class Choice(Base):
    "DB Choice"
    __tablename__ = 'choice'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    choice_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    category = sa.Column(sa.String(50), nullable=False, index=True)
    text = sa.Column(sa.String())
    value = sa.Column(Json)

class UserRole(Base):
    "DB User Role"
    __tablename__ = 'user_role'
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), primary_key=True)
    role = sa.Column(sa.String(50), primary_key=True)    # user role

class User(Base):
    "DB User"
    __tablename__ = 'user'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    user_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    utype = sa.Column(sa.String(15), index=True)                # user type
    name = sa.Column(sa.String(), nullable=False)               # display name
    initial = sa.Column(sa.String(), default='')                # display initial name
    password = sa.Column(sa.String(), nullable=False)           # plain password
    email = sa.Column(sa.String(255), nullable=False, index=True, unique=True)  # email (unique)
    mobile = sa.Column(sa.String(), default='')                 # mobile
    disabled = sa.Column(sa.Boolean, default=False)             # disabled
    #roles = sa.Column(Json, default=[])                         # user role list
    is_deleted = sa.Column(sa.Boolean, default=False)              # mark delete

    # timestamp
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    updated_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())

    # relationship
    roles = relationship('UserRole', lazy=True, cascade="all, delete-orphan")   # user role list

    # polymorphic mapping by multiple tables
    __mapper_args__ = {
        'polymorphic_identity': 'user',
        'polymorphic_on': utype
    }

# student course association mapping
class StudentCourse(Base):
    "DB student course"
    __tablename__ = 'student_course'
    course_id = sa.Column(sa.Integer, sa.ForeignKey('course.course_id'), primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('student.user_id'), primary_key=True)
    # course = relationship("Course", uselist=False, foreign_keys=[course_id], lazy=True)
    student = relationship("Student", uselist=False, foreign_keys=[user_id], lazy=True)

#student lesson association mapping
class StudentLesson(Base):
    "DB student lesson"
    __tablename__ = 'student_lesson'
    lesson_id = sa.Column(sa.Integer, sa.ForeignKey('lesson.lesson_id'), primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('student.user_id'), primary_key=True)
    # lesson = relationship("Lesson", uselist=False, foreign_keys=[lesson_id], lazy=True)
    # student = relationship("Student", uselist=False, foreign_keys=[user_id], lazy=True)
    attendance = sa.Column(sa.String())


class Student(User):
    "DB Student"
    __tablename__ = 'student'
    __mapper_args__ = { 'polymorphic_identity': 'student' } # polymorphic to User
    # __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    # student_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), primary_key=True)
    student_code = sa.Column(sa.String(50), index=True)
    trial_student_code = sa.Column(sa.String())
    referral_code = sa.Column(sa.String())
    referral_name = sa.Column(sa.String())
    # first_name = sa.Column(sa.String())
    # last_name = sa.Column(sa.String())
    # alias_name = sa.Column(sa.String())
    # name = sa.Column(sa.String())
    parent_1 = sa.Column(sa.String())
    parent_2 = sa.Column(sa.String())
    mobile_whatsapp = sa.Column(sa.String())
    mobile_classin = sa.Column(sa.String())
    dob = sa.Column(sa.Date)
    gender = sa.Column(sa.String())
    classin_uid = sa.Column(sa.Integer)              # classin UID
    # age = sa.Column(sa.Integer)
    # email = sa.Column(sa.String())          # email

    # relationships
    _sc = relationship('StudentCourse', lazy=True, cascade="all, delete-orphan")
    courses = association_proxy('_sc', attr='course', creator=lambda x: StudentCourse(course_id=x))


class TeacherProduct(Base):
    "DB Teacher Product"
    __tablename__ = 'teacher_product'
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), primary_key=True)
    product_id = sa.Column(sa.Integer, sa.ForeignKey('product.product_id'), primary_key=True)
    teacher = relationship("Teacher", uselist=False, foreign_keys=[user_id], lazy=True)
    product = relationship("Product", uselist=False, foreign_keys=[product_id], lazy=True)


class Teacher(User):
    "DB Teacher"
    __tablename__ = 'teacher'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    __mapper_args__ = { 'polymorphic_identity': 'teacher' } # polymorphic to User
    # teacher_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), primary_key=True)
    teacher_code = sa.Column(sa.String())
    # name = sa.Column(sa.String())
    mobile_whatsapp = sa.Column(sa.String())
    mobile_classin = sa.Column(sa.String())
    gender = sa.Column(sa.String())
    hourly_rate = sa.Column(sa.Numeric(asdecimal=False))
    trial_bonus = sa.Column(sa.Numeric(asdecimal=False))
    classin_uid = sa.Column(sa.Integer)              # classin UID

    # relationships
    _tp = relationship('TeacherProduct', lazy=True, cascade="all, delete-orphan")
    products = association_proxy('_tp', attr='product', creator=lambda x: TeacherProduct(product_id=x))


class Staff(User):
    "DB Staff"
    __tablename__ = 'staff'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    __mapper_args__ = { 'polymorphic_identity': 'staff' } # polymorphic to User
    user_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), primary_key=True)
    staff_code = sa.Column(sa.String())
    gender = sa.Column(sa.String())
    # mobile_whatsapp = sa.Column(sa.String())
    # mobile_classin = sa.Column(sa.String())

# student_alias = aliased(User, name="student")
# teacher_alias = aliased(User, name="teacher")
# staff_alias = aliased(User, name="staff")
# student_alias = AliasedClass(User, name="student")
# teacher_alias = AliasedClass(User, name="teacher")
# staff_alias = AliasedClass(User, name="staff")


class Product(Base):
    "DB Course Product"
    __tablename__ = 'product'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    product_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    product_code = sa.Column(sa.String())
    name = sa.Column(sa.String())
    trial = sa.Column(sa.Boolean)
    category = sa.Column(sa.String())
    language = sa.Column(sa.String())
    formation = sa.Column(sa.String())
    level = sa.Column(sa.String())
    lesson_price = sa.Column(sa.Numeric(asdecimal=False))
    is_deleted = sa.Column(sa.Boolean, default=False)              # mark delete
    # timestamp
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    updated_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())

    # relationships
    # teachers = relationship('Teacher', secondary=teacher_product_association, lazy=True)
    # _tp = relationship('TeacherProduct', lazy=True, cascade="all, delete-orphan")
    # teachers = association_proxy('_tp', attr='teacher', creator=lambda x: TeacherProduct(user_id=x))


class Course(Base):
    "DB Course"
    __tablename__ = 'course'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    course_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    product_id = sa.Column(sa.Integer, sa.ForeignKey('product.product_id'), nullable=False)
    teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), nullable=True)        # main teacher
    # subject_id = sa.Column(sa.Integer, sa.ForeignKey('subject.subject_id'), nullable=False)
    # subject = sa.Column(sa.String())
    # formation = sa.Column(sa.String())
    # level = sa.Column(sa.String())
    # payment_reference = sa.Column(sa.String())
    course_code = sa.Column(sa.String())
    name = sa.Column(sa.String())
    course_intro = sa.Column(sa.String())               # course introduction
    status = sa.Column(sa.String())                     # course status
    paid_classes = sa.Column(sa.Integer)
    free_classes = sa.Column(sa.Integer)
    free_group_classes = sa.Column(sa.Integer)
    # combined expression field: paid_classes + free_classes + free_group_classes
    # total_classes = query_expression(paid_classes.op('+')(free_classes.op('+')(free_group_classes)))
    total_classes = column_property(paid_classes + free_classes + free_group_classes)
    # special_promo_code = sa.Column(sa.String())
    lesson_first_date = sa.Column(sa.Date)
    end_date = sa.Column(sa.Date)
    is_deleted = sa.Column(sa.Boolean, default=False)              # mark delete
    # timestamp
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    updated_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    # classin
    classin_courseid = sa.Column(sa.Integer)              # classin course id

    # relationships
    product = relationship('Product', uselist=False, lazy=True, foreign_keys=[product_id])
    teacher = relationship('Teacher', uselist=False, lazy=True, foreign_keys=[teacher_id])
    _sc = relationship('StudentCourse', lazy=True, cascade="all, delete-orphan")
    students = association_proxy('_sc', attr='student', creator=lambda x: StudentCourse(user_id=x))
    lessons = relationship('Lesson', lazy=True, cascade="all, delete-orphan")


class Lesson(Base):
    __tablename__ = 'lesson'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    lesson_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    course_id = sa.Column(sa.Integer, sa.ForeignKey('course.course_id'), nullable=False)
    teacher_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), nullable=True)     # actual teacher
    ta_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), nullable=True)          # TA
    lesson_number = sa.Column(sa.Integer)
    status = sa.Column(sa.String())                     # course status
    is_free = sa.Column(sa.Boolean, default=False)
    schedule_start = sa.Column(sa.DateTime)
    schedule_end = sa.Column(sa.DateTime)
    start_time = sa.Column(sa.DateTime)
    end_time = sa.Column(sa.DateTime)
    # duration
    # student_attended = sa.Column(Json, default=[])       # student ID list attended
    # student_absent = sa.Column(Json, default=[])         # student ID list absent
    # timestamp
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    updated_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    # classin
    classin_classid = sa.Column(sa.Integer)              # classin class id

    # relationships
    course = relationship('Course', uselist=False, lazy=True, foreign_keys=[course_id])
    teacher = relationship('Teacher', uselist=False, lazy=True, foreign_keys=[teacher_id])
    ta = relationship('Staff', uselist=False, lazy=True, foreign_keys=[ta_id])
    student_actions = relationship('StudentLesson', lazy=True, cascade="all, delete-orphan")
    # _sl = relationship('StudentLesson', lazy=True, cascade="all, delete-orphan")
    # students = association_proxy('_sl', attr='student', creator=lambda x: StudentLesson(user_id=x))

    # additional properties from Course
    course_name = column_property(Course.name)
    course_code = column_property(Course.course_code)
    total_classes = column_property(Course.total_classes)

    @property
    def students(self):
        return self.course.students

# map course lessons
# class_mapper(Course, properties={
#   'lessons': relationship('Lessson', lazy=True, foreign_keys=[Course.course_id], cascade="all, delete-orphan")
# })

class SalesOrder(Base):
    "DB SalesOrder"
    __tablename__ = 'sales_order'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    # [sales order]
    so_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    sales_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), nullable=False)  # sales user id
    course_id = sa.Column(sa.Integer, sa.ForeignKey('course.course_id'), nullable=False) # course belong to
    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'), nullable=False) # student belong to
    # payment_id = sa.Column(sa.Integer, sa.ForeignKey('payment.payment_id'), nullable=True) # payment belong to
    so_number = sa.Column(sa.String(50), index=True)
    so_date = sa.Column(sa.Date)
    status = sa.Column(sa.String())
    existing = sa.Column(sa.Boolean, default=False)
    promo_code = sa.Column(sa.String())
    currency = sa.Column(sa.String())
    so_amount = sa.Column(sa.Numeric(asdecimal=False), default=0)
    ex_rate = sa.Column(sa.Numeric(asdecimal=False), default=0)
    so_gross = sa.Column(sa.Numeric(asdecimal=False), default=0)
    so_discount = sa.Column(sa.Numeric(asdecimal=False), default=0)
    so_net = sa.Column(sa.Numeric(asdecimal=False), default=0)
    # course_value = sa.Column(sa.Numeric(asdecimal=False), default=0)
    so_issued = sa.Column(sa.Boolean, default=False)
    is_deleted = sa.Column(sa.Boolean, default=False)              # mark delete
    # [timestamp]
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    updated_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())

    # relationships
    sales = relationship('Staff', uselist=False, lazy=True, foreign_keys=[sales_id])
    course = relationship('Course', uselist=False, lazy=True)
    student = relationship('Student', uselist=False, lazy=True, foreign_keys=[student_id])
    payment = relationship('Payment', uselist=False, lazy=True)

    # aliases (needed for join with User table multiple times)
    _student = aliased(User, name="student")
    _sales = aliased(User, name="sales")

    # additional properties from Staff, Course, Student
    course_name = column_property(Course.name)
    sales_name = column_property(_sales.name)
    student_name = column_property(_student.name)


class Payment(Base):
    "DB Payment"
    __tablename__ = 'payment'
    __table_args__ = {'sqlite_autoincrement': True}     # auto increment for SQLite
    payment_id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    so_id = sa.Column(sa.Integer, sa.ForeignKey('sales_order.so_id'), nullable=False)
    # [payment]
    received_date = sa.Column(sa.Date)
    received_amount = sa.Column(sa.Numeric(asdecimal=False))
    payee_name = sa.Column(sa.String())
    payee_ref = sa.Column(sa.String())
    bank_account = sa.Column(sa.String())
    bank_ref = sa.Column(sa.String())
    received_by = sa.Column(sa.Integer, sa.ForeignKey('user.user_id'))
    receipt_no = sa.Column(sa.String())
    receipt_date = sa.Column(sa.Date)
    # [additional]
    account_code = sa.Column(sa.String())
    signature_approval = sa.Column(sa.String())
    payment_method = sa.Column(sa.String())
    last_contact_date = sa.Column(sa.Date)
    attrition_code = sa.Column(sa.String())
    remarks = sa.Column(sa.String())
    is_deleted = sa.Column(sa.Boolean, default=False)              # mark delete
    # [timestamp]
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())
    updated_at = sa.Column(sa.DateTime, nullable=False, default=datetime.now())

    # relationship
    # salesorder = relationship('SalesOrder', backref='payment', uselist=False, 
    #     lazy=True, foreign_keys=[so_id])
    sales_order = relationship('SalesOrder', uselist=False, lazy=True)
    receiver = relationship('Staff', uselist=False, lazy=True, foreign_keys=[received_by])

    # additional properties from SalesOrder, Staff
    so_number = column_property(SalesOrder.so_number)
    so_date = column_property(SalesOrder.so_date)
    so_amount = column_property(SalesOrder.so_amount)
    so_status = column_property(SalesOrder.status)
    receiver_name = column_property(User.name)

    @property
    def student(self):
        return self.sales_order.student

    @property
    def sales(self):
        return self.sales_order.sales
