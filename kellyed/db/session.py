from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session, scoped_session
from ..core.config import settings
import json

is_sqlite = settings.sqlalchemy_database_uri.startswith('sqlite:')
is_postgre = settings.sqlalchemy_database_uri.startswith('postgresql')
is_mssql = settings.sqlalchemy_database_uri.startswith('mssql')
is_mysql = settings.sqlalchemy_database_uri.startswith('mysql')

# As SQLAlchemy has its own pooling behavior, it is often preferable to disable pyodbc pooling
if is_mssql:
    import pyodbc
    pyodbc.pooling = False

# engine arguments
engine_args = {}
if is_sqlite:
    engine_args = {
        'convert_unicode': False,
        'connect_args': {"check_same_thread": True},                        # useful for sqlite only
        'json_serializer': lambda obj: json.dumps(obj, ensure_ascii=False)  # no convert unicode to ascii
    }

engine = create_engine(settings.sqlalchemy_database_uri, echo=settings.sqlalchemy_echo, **engine_args)
SessionLocal: Session = scoped_session(sessionmaker(bind=engine, autoflush=False, autocommit=False))
Base = declarative_base()

async def get_db() -> Session:
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()