from typing import List
from datetime import datetime
from .session import Session, Base, is_mssql
from sqlalchemy.orm import Query
from kellyed.core import utils
import logging

logger = logging.getLogger(__name__)

# Base CRUD
class BaseCrud:
    name: str
    model: Base = None
    # sub_items are properties need to convert between model and schema
    sub_items: dict = {}
    join_models: List[Base] = []

    def __init__(self, name: str, model: Base, 
                sub_items={}, join_models: List[Base]=[]) -> None:
        self.name = name
        self.model = model
        self.sub_items = sub_items
        self.join_models = join_models

    def prepare(self, db: Session, join=False) -> Query:
        dbo = db.query(self.model)
        # join relationship models if required
        if join:
            for model in self.join_models:
                if isinstance(model, list):
                    dbo = dbo.join(*model)
                else:
                    dbo = dbo.join(model)
        return dbo

    def schema_dict(self, schema):
        data = schema.dict(by_alias=True, exclude_unset=True, exclude=set(self.sub_items))
        for key, item_class in self.sub_items.items():
            if hasattr(schema, key):
                data[key] = [item_class(**x.dict()) for x in getattr(schema, key)]
        return data

    def create(self, db: Session, item):
        "db create item"
        data = self.schema_dict(item)
        db_item = self.model(**data)
        if hasattr(db_item, 'created_at'):
            db_item.created_at = datetime.now()
        if hasattr(db_item, 'updated_at'):
            db_item.updated_at = datetime.now()
        db.add(db_item)
        db.commit()
        return db_item

    def update(self, db: Session, id: int, item):
        "db update"
        db_item = db.query(self.model).get(id)
        if not db_item:
            raise utils.NotFound(detail=f"{self.name} not found")
        if hasattr(self.model, 'is_deleted') and db_item.is_deleted:
            raise utils.NotFound(detail=f"{self.name} is deleted")
        update_data = self.schema_dict(item)
        for key, value in update_data.items():
            setattr(db_item, key, value)
        if hasattr(db_item, 'updated_at'):
            db_item.updated_at = datetime.now()
        db.add(db_item)
        db.commit()
        db.refresh(db_item)
        return db_item

    def read(self, db: Session, id: int=0, **kw):
        "db read"
        if len(kw) > 0:
            return db.query(self.model).filter_by(**kw).one_or_none()
        else:
            return db.query(self.model).get(id)

    def delete(self, db: Session, id: int):
        "db delete"
        db_item = db.query(self.model).get(id)
        if not db_item:
            raise utils.NotFound(detail=f"{self.name} not found")
        if hasattr(self.model, 'is_deleted') and db_item.is_deleted:
            raise utils.NotFound(detail=f"{self.name} is deleted")
        db.delete(db_item)
        db.commit()

    def count(self, db: Session, filter_func=None, join=False, **kw) -> int:
        "db list count"
        dbo = self.prepare(db, join=join)
        if filter_func:
            dbo = filter_func(dbo, **kw)
        return dbo.count()

    def list(self, db: Session, offset=0, limit=0, order_by=None, order_desc=False, 
            filter_func=None, join=False, **kw):
        "db list"
        if join:
            dbo = self.prepare(db, join=join)
        else:
            dbo = db.query(self.model)
        if filter_func:
            dbo = filter_func(dbo, **kw)
        if order_by and hasattr(self.model, order_by):
            field = getattr(self.model, order_by)
            if order_desc:
                field = field.desc()
            dbo = dbo.order_by(field)
        elif is_mssql and offset > 0:       # MSSQL require order_by primiary keys for offset
            dbo = dbo.order_by(*self.model.__table__.primary_key)
        if offset > 0:
            dbo = dbo.offset(offset)
        if limit > 0:
            dbo = dbo.limit(limit)
        return dbo.all()
