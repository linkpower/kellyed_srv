from os import chdir, path
from fastapi import FastAPI, Request, Response, HTTPException
from fastapi.param_functions import Depends
from fastapi.responses import RedirectResponse, JSONResponse
from fastapi.exceptions import RequestValidationError
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.openapi.docs import (
    get_swagger_ui_html, 
    get_swagger_ui_oauth2_redirect_html, 
    get_redoc_html
)
import uvicorn
import logging
from kellyed.db.models import db_create_all
from kellyed.core.config import settings
from kellyed.core import utils, oauth2
from kellyed.api import root, auth, ws, choices, admin, staffs, students, teachers, products
from kellyed.api import courses, lessons, salesorders, payments

logger = logging.getLogger(__name__)

# create fastapi app
app = FastAPI(
    title="KellyEd Service",
    version=settings.version,
    docs_url=None,      # "/api/docs",
    redoc_url=None,     # "/api/redoc",
    openapi_url="/api/openapi.json",
    # servers=[{"url": "/api"}]
)

# custom exception handler
@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    if exc.status_code >= 500:
        logger.exception(exc)
    else:
        request_path = utils.get_request_path(request)
        logger.error(f"http_exception_handler: {request_path} [{exc.detail}]")
    return JSONResponse({
        'status': exc.status_code,
        'detail': exc.detail
    }, status_code=exc.status_code)

@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request, exc):
    logger.error(f"validation_exception_handler: {str(exc)}")
    return JSONResponse({
        'status': 400,
        'detail': str(exc)
    }, status_code=400)

# redirect / to /static/
@app.get("/", include_in_schema=False)
async def get_index():
    "redirect index to /static/"
    return RedirectResponse("/static/")

# mount /static as static file path
app.mount("/static", StaticFiles(directory="static", html=True), name="static")
# add CORS middleware
async def catch_exceptions_middleware(request: Request, call_next):
    try:
        return await call_next(request)
    except HTTPException as e:
        logger.exception(e)
        return e
    except Exception as e:
        # you probably want some kind of logging here
        logger.exception(e)
        return Response("Internal server error", status_code=500)
app.middleware('http')(catch_exceptions_middleware)
app.add_middleware(
    CORSMiddleware, 
    allow_origins=settings.cors_origins, 
    allow_credentials=True, 
    allow_methods=['*'],
    allow_headers=['*'],
)

# custom swagger UI HTML
@app.get("/api/docs", include_in_schema=False)
async def custom_swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=app.openapi_url,
        title=f"{app.title} - OpenAPI",
        oauth2_redirect_url=app.swagger_ui_oauth2_redirect_url,
        swagger_js_url="/static/doc/swagger-ui-bundle.js",
        swagger_css_url="/static/doc/swagger-ui.css",
        swagger_favicon_url=None
    )

# swagger UI redirect
@app.get(app.swagger_ui_oauth2_redirect_url, include_in_schema=False)
async def swagger_ui_redirect():
    return get_swagger_ui_oauth2_redirect_html()

# custom redoc HTML
@app.get("/api/redoc", include_in_schema=False)
async def custom_redoc_html():
    return get_redoc_html(
        openapi_url=app.openapi_url,
        title=f"{app.title} - ReDoc",
        redoc_js_url="/static/doc/redoc.standalone.js",
        redoc_favicon_url=None
    )

def app_init(project_path: str=None, logger_config: dict=None):
    "app initialize"

    # change to project path as root
    if project_path is None:
        project_path = path.dirname(path.dirname(path.realpath(__file__)))
    chdir(project_path)

    # set app config
    # config_name = environ.get('APP_SETTINGS', 'config.DevConfig')
    # app.config.from_object(config_name)
    # app.config.from_pyfile(path.join(project_path, 'cfg', 'server_common.cfg'))
    # for cfg_name in more_cfg:
    #     app.config.from_pyfile(path.join(project_path, cfg_name))
    # setup logger
    if logger_config is None:
        logger_config = utils.load_json(settings.log_cfg)
    # change level when has DEBUG flag 
    if settings.debug:
        for i, clogger in logger_config['loggers'].items():
            clogger['level'] = 'DEBUG'
    logging.config.dictConfig(logger_config)
    if settings.env != 'testing':
        logger.info("-" * 60)               # init log
    # create necessary path
    utils.create_dirs(settings.instance_path)
    utils.create_dirs(settings.log_folder)
    utils.create_dirs(settings.upload_folder)
    if settings.upload_clean:
        utils.clear_folder(settings.upload_folder)

    # create database if db file or tables not exist
    db_create_all()

@app.on_event("startup")
async def startup_event():
    "init app on startup"
    project_path = path.dirname(path.dirname(path.realpath(__file__)))
    app_init(project_path)
    logger.info("started '%s' environment: http://%s:%d/", 
        settings.env, settings.listen_host, settings.listen_port)

# define routing
app.include_router(root.router, prefix="/api")
app.include_router(auth.router, prefix="/api/auth", tags=["auth"])

# protected API
# disabled for testing environment
protected_depends = [Depends(oauth2.get_payload)] if settings.env != 'testing' else None
app.include_router(admin.router, prefix="/api/admin", tags=["admin"],
    dependencies=protected_depends)
app.include_router(choices.router, prefix="/api/choices", tags=["choices"],
    dependencies=protected_depends)
app.include_router(staffs.router, prefix="/api/staffs", tags=["staffs"],
    dependencies=protected_depends)
app.include_router(students.router, prefix="/api/students", tags=["students"],
    dependencies=protected_depends)
app.include_router(teachers.router, prefix="/api/teachers", tags=["teachers"],
    dependencies=protected_depends)
app.include_router(products.router, prefix="/api/products", tags=["products"],
    dependencies=protected_depends)
app.include_router(courses.router, prefix="/api/courses", tags=["courses"],
    dependencies=protected_depends)
app.include_router(lessons.router, prefix="/api/lessons", tags=["lessons"],
    dependencies=protected_depends)
app.include_router(salesorders.router, prefix="/api/salesorders", tags=["salesorders"],
    dependencies=protected_depends)
app.include_router(payments.router, prefix="/api/payments", tags=["payments"],
    dependencies=protected_depends)
# app.include_router(users.router, prefix="/api/users", tags=["users"],
#     dependencies=protected_depends)

# websocket router
ws.manager.set_app(app)
app.include_router(ws.router, prefix="/ws", tags=["websocket"])

if __name__ == "__main__":
    uvicorn.run("kellyed.main:app", host=settings.listen_host, 
        port=settings.listen_port, reload=True)