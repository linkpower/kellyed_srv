import sqlalchemy as sa
from sqlalchemy.orm import relationship, deferred
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.sql.expression import or_, and_, not_, asc, desc
from sqlalchemy import func
from datetime import datetime
from ..db.session import Base, engine


class ClassinUser(Base):
    "DB Classin User"
    __tablename__ = 'classin_user'
    user_id = sa.Column(sa.Integer, primary_key=True)           # kelly db user id
    uid = sa.Column(sa.Integer, index=True, nullable=False)     # classin user UID
    telephone = sa.Column(sa.String())                          # mobile
    nickname = sa.Column(sa.String())                           # nickname
    password = sa.Column(sa.String())                           # text password
    md5pass = sa.Column(sa.String())                            # MD5 password


class ClassinStudent(Base):
    "DB Classin Student"
    __tablename__ = 'classin_student'
    kuser_id = sa.Column(sa.Integer, primary_key=True)              # kelly db user id
    studentUid = sa.Column(sa.Integer, index=True, nullable=False)  # classin student UID
    studentName = sa.Column(sa.String())                            # student name


class ClassinTeacher(Base):
    "DB Classin Teacher"
    __tablename__ = 'classin_teacher'
    kuser_id = sa.Column(sa.Integer, primary_key=True)              # kelly db user id
    teacherUid = sa.Column(sa.Integer, index=True, nullable=False)  # classin teacher UID
    teacherName = sa.Column(sa.String())                            # teacher name


class ClassinCourse(Base):
    "DB Classin Teacher"
    __tablename__ = 'classin_course'
    kcourse_id = sa.Column(sa.Integer, primary_key=True)            # kelly db course id
    courseId = sa.Column(sa.Integer, index=True, nullable=False)    # classin course ID
    courseName = sa.Column(sa.String())
    folderId = sa.Column(sa.Integer)
    expiryTime = sa.Column(sa.Integer)
    #courseIntroduce = sa.Column(sa.String())
    classroomSettingId = sa.Column(sa.Integer)
    #courseUniqueIdentity = sa.Column(sa.String())
    mainTeacherUid = sa.Column(sa.Integer)


class ClassinClass(Base):
    "DB Classin Teacher"
    __tablename__ = 'classin_course'
    kcourse_id = sa.Column(sa.Integer, primary_key=True)            # kelly db course id
    courseId = sa.Column(sa.Integer, index=True, nullable=False)    # classin course ID
    className = sa.Column(sa.String())
    folderId = sa.Column(sa.Integer)
    beginTime = sa.Column(sa.Integer)
    endTime = sa.Column(sa.Integer)
    teacherUid = sa.Column(sa.Integer)
    folderId = sa.Column(sa.Integer)
    seatNum = sa.Column(sa.Integer)
    record = sa.Column(sa.Integer)
    live = sa.Column(sa.Integer)
    replay = sa.Column(sa.Integer)
    assistantUid = sa.Column(sa.Integer)
    isAutoOnstage = sa.Column(sa.Integer)
    isHd = sa.Column(sa.Integer)
    #courseUniqueIdentity = sa.Column(sa.String())
    #classIntroduce = sa.Column(sa.String())


class ClassinCourseStudent(Base):
    "DB Classin Course Student"
    __tablename__ = 'classin_course_student'
    kcourse_id = sa.Column(sa.Integer, primary_key=True)            # kelly db course id
    kuser_id = sa.Column(sa.Integer, primary_key=True)              # kelly db user id
    courseId = sa.Column(sa.Integer, nullable=False)                # classin course ID
    studentUid = sa.Column(sa.Integer)
    #identity = sa.Column(sa.Integer)
    #studentName = sa.Column(sa.String())


class ClassinClassStudent(Base):
    "DB Classin Class Student"
    __tablename__ = 'classin_class_student'
    klesson_id = sa.Column(sa.Integer, primary_key=True)            # kelly db lesson id
    kuser_id = sa.Column(sa.Integer, primary_key=True)              # kelly db user id
    courseId = sa.Column(sa.Integer, nullable=False)                # classin course ID
    classId = sa.Column(sa.Integer, nullable=False)                 # classin class ID
    identity = sa.Column(sa.Integer)                                # Students identify(1 for the students)
    #isRegister = sa.Column(sa.Integer)
    #studentJson = sa.Column(sa.JSON())                             # list of student {uid, name, customColumn}

