from ..db import models

role_creator = lambda x: models.UserRole(role=x)
# sheet information for import/export
# must in order of dependency for import
sheet_info = {
    'Choice': {
        'model': models.Choice,
        'key': 'choice_id',
        'headers': [
            {'field': 'choice_id', 'label': 'Choice ID', 'row_number': True},
            {'field': 'category', 'label': 'Category'},
            {'field': 'text', 'label': 'Label'},
            {'field': 'value', 'label': 'Value'},
        ]
    },
    'Staff': {
        'model': models.Staff,
        'key': 'user_id',
        'headers': [
            {'field': 'user_id', 'label': 'Staff ID'},
            {'field': 'name', 'label': 'Staff Name'},
            {'field': 'initial', 'label': 'Name Initial'},
            {'field': 'password', 'label': 'Password'},
            {'field': 'email', 'label': 'Email'},
            {'field': 'mobile', 'label': 'Mobile'},
            {'field': 'disabled', 'label': 'Disabled'},
            {'field': 'roles', 'label': 'User Roles', 'to_list': role_creator},
            {'field': 'staff_code', 'label': 'Staff Code'},
            {'field': 'gender', 'label': 'Gender'},
        ],
        'relationship': [
            {'model': models.User, 'filter_by': {'utype': 'staff'}}
        ]
    },
    'Student': {
        'model': models.Student,
        'key': 'user_id',
        'headers': [
            {'field': 'user_id', 'label': 'Student ID'},
            {'field': 'name', 'label': 'Student Name'},
            {'field': 'initial', 'label': 'Name Initial'},
            {'field': 'password', 'label': 'Password'},
            {'field': 'email', 'label': 'Email'},
            {'field': 'mobile', 'label': 'Mobile'},
            {'field': 'disabled', 'label': 'Disabled'},
            {'field': 'roles', 'label': 'User Roles', 'to_list': role_creator},
            {'field': 'student_code', 'label': 'Student Code'},
            {'field': 'trial_student_code', 'label': 'Trial Student Code'},
            {'field': 'referral_code', 'label': 'Referral Code'},
            {'field': 'referral_name', 'label': 'Referral Name'},
            {'field': 'parent_1', 'label': 'Parent 1 Name'},
            {'field': 'parent_2', 'label': 'Parent 2 Name'},
            {'field': 'mobile_whatsapp', 'label': 'Mobile Whatsapp'},
            {'field': 'mobile_classin', 'label': 'Mobile Class-In'},
            {'field': 'dob', 'label': 'Date of Birth'},
            {'field': 'gender', 'label': 'Gender'},
        ],
        'relationship': [
            {'model': models.User, 'filter_by': {'utype': 'student'}}
        ]
    },
    'Teacher': {
        'model': models.Teacher,
        'key': 'user_id',
        'headers': [
            {'field': 'user_id', 'label': 'Teacher ID'},
            {'field': 'name', 'label': 'Teacher Name'},
            {'field': 'initial', 'label': 'Name Initial'},
            {'field': 'password', 'label': 'Password'},
            {'field': 'email', 'label': 'Email'},
            {'field': 'mobile', 'label': 'Mobile'},
            {'field': 'disabled', 'label': 'Disabled'},
            {'field': 'roles', 'label': 'User Roles', 'to_list': role_creator},
            {'field': 'teacher_code', 'label': 'Teacher Code'},
            {'field': 'mobile_whatsapp', 'label': 'Mobile Whatsapp'},
            {'field': 'mobile_classin', 'label': 'Mobile Class-In'},
            {'field': 'gender', 'label': 'Gender'},
            {'field': 'hourly_rate', 'label': 'Hourly Rate'},
            {'field': 'trial_bonus', 'label': 'Trial Bonus'},
        ],
        'relationship': [
            {'model': models.User, 'filter_by': {'utype': 'teacher'}},
            {'model': models.TeacherProduct},
        ]
    },
    'Product': {
        'model': models.Product,
        'key': 'product_id',
        'headers': [
            {'field': 'product_id', 'label': 'Product ID'},
            {'field': 'product_code', 'label': 'Product Code'},
            {'field': 'name', 'label': 'Product Name'},
            {'field': 'trial', 'label': 'Trial Course'},
            {'field': 'category', 'label': 'Category'},
            {'field': 'language', 'label': 'Language'},
            {'field': 'formation', 'label': 'Formation'},
            {'field': 'level', 'label': 'Level'},
            {'field': 'lesson_price', 'label': 'Lesson Price'},
        ],
        'relationship': [
            {'model': models.TeacherProduct},
        ]
    },
    'Teacher Product': {
        'model': models.TeacherProduct,
        'key': ['user_id', 'product_id'],
        'headers': [
            {'field': 'user_id', 'label': 'Teacher ID'},
            {'field': 'product_id', 'label': 'Product ID'},
        ],
    },
    'Course': {
        'model': models.Course,
        'key': 'course_id',
        'headers': [
            {'field': 'course_id', 'label': 'Course ID'},
            {'field': 'product_id', 'label': 'Product ID'},
            {'field': 'teacher_id', 'label': 'Teacher ID'},
            {'field': 'course_code', 'label': 'Course Code'},
            {'field': 'name', 'label': 'Course Name'},
            {'field': 'course_intro', 'label': 'Course Introduction'},
            {'field': 'status', 'label': 'Status'},
            {'field': 'paid_classes', 'label': 'Paid Classes'},
            {'field': 'free_classes', 'label': 'Free Classes'},
            {'field': 'free_group_classes', 'label': 'Free Group Classes'},
            {'field': 'lesson_first_date', 'label': 'Lesson First Date'},
            {'field': 'end_date', 'label': 'Course End Date'},
            {'field': 'classin_courseid', 'label': 'Classin Course ID'},
        ],
        'relationship': [
            {'model': models.StudentCourse},
        ]
    },
    'Student Course': {
        'model': models.StudentCourse,
        'key': ['course_id', 'user_id'],
        'headers': [
            {'field': 'course_id', 'label': 'Course ID'},
            {'field': 'user_id', 'label': 'Student ID'},
        ],
    },
    'Lesson': {
        'model': models.Lesson,
        'key': 'lesson_id',
        'headers': [
            {'field': 'lesson_id', 'label': 'Lesson ID'},
            {'field': 'course_id', 'label': 'Course ID'},
            {'field': 'teacher_id', 'label': 'Teacher ID'},
            {'field': 'ta_id', 'label': 'TA ID'},
            {'field': 'lesson_number', 'label': 'Lesson Number'},
            {'field': 'is_free', 'label': 'Free Lesson'},
            {'field': 'status', 'label': 'Status'},
            {'field': 'schedule_start', 'label': 'Schedule Start Time'},
            {'field': 'schedule_end', 'label': 'Schedule End Time'},
            {'field': 'start_time', 'label': 'Start Time'},
            {'field': 'end_time', 'label': 'End Time'},
            {'field': 'classin_classid', 'label': 'Classin Class ID'},
        ],
        'relationship': [
            {'model': models.StudentLesson},
        ]
    },
    'Student Lesson': {
        'model': models.StudentLesson,
        'key': ['lesson_id', 'user_id'],
        'headers': [
            {'field': 'lesson_id', 'label': 'Lesson ID'},
            {'field': 'user_id', 'label': 'Student ID'},
            {'field': 'attendance', 'label': 'Attendance'},
        ],
    },
    'Sales Order': {
        'model': models.SalesOrder,
        'key': 'so_id',
        'headers': [
            {'field': 'so_id', 'label': 'Sales Order ID'},
            {'field': 'sales_id', 'label': 'Sales ID'},
            {'field': 'course_id', 'label': 'Course ID'},
            {'field': 'student_id', 'label': 'Student ID'},
            {'field': 'so_number', 'label': 'Sales Order Number'},
            {'field': 'so_date', 'label': 'SO Date'},
            {'field': 'status', 'label': 'SO Status'},
            {'field': 'existing', 'label': 'New vs. Existing'},
            {'field': 'promo_code', 'label': 'Special Promo Code'},
            {'field': 'currency', 'label': 'Currency'},
            {'field': 'so_amount', 'label': 'SO Amount (original)'},
            {'field': 'ex_rate', 'label': 'Ex. Rate'},
            {'field': 'so_gross', 'label': 'SO GROSS Amount (HK$)'},
            {'field': 'so_discount', 'label': 'Other Discount (HK$)'},
            {'field': 'so_net', 'label': 'SO NET Amount (HK$)'},
            # {'field': 'course_value', 'label': 'Sales value per course (HK$)', 'export_only': True},
            {'field': 'so_issued', 'label': 'Sales Order confirmation issued'},
        ]
    },
    'Payment': {
        'model': models.Payment,
        'key': 'so_id',
        'headers': [
            {'field': 'payment_id', 'label': 'Payment ID'},
            {'field': 'so_id', 'label': 'Sales Order ID'},
            {'field': 'received_date', 'label': 'Cash Received Date'},
            {'field': 'received_amount', 'label': 'Cash Received Amount'},
            {'field': 'payee_name', 'label': 'Payee name'},
            {'field': 'payee_ref', 'label': 'Payee reference'},
            {'field': 'bank_account', 'label': 'Bank Account'},
            {'field': 'bank_ref', 'label': 'Bank payment reference'},
            {'field': 'received_by', 'label': 'Bank received checked by'},
            {'field': 'receipt_no', 'label': 'Sales Receipt Issued'},
            {'field': 'receipt_date', 'label': 'Sales Receipt Date'},
            {'field': 'account_code', 'label': 'Account Code'},
            {'field': 'signature_approval', 'label': 'Electronic Signature of Approval'},
            {'field': 'payment_method', 'label': 'Payment Method'},
            {'field': 'last_contact_date', 'label': 'Last Sales contact date'},
            {'field': 'attrition_code', 'label': 'Attrition Code'},
            {'field': 'remarks', 'label': 'Remarks'},
        ]
    },
}