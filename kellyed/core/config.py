from sys import version
from typing import List, Dict
from pydantic import BaseSettings, validator
# import cryptocode

class Settings(BaseSettings):
    version = "0.2.0"                       # KellyEdSrv version
    cipher_key = b'6X1-EopY7vSZRRdudoGKp8_B2kqjnfJ4Y3CurAfWVTg='     # encryption cipher key
    env = 'development'                     # runtime environment
    debug = False                           # debug flag
    testing = False                         # testing flag
    listen_host = 'localhost'               # listen host
    listen_port = 8100                      # listen port
    instance_path = './instance'            # instance folder
    batch_folder = './instance/batch'       # instance batch folder
    log_folder = './instance/log'           # instance log folder
    upload_folder = './instance/upload'     # instance upload folder
    static_folder = './static'              # static HTML folder
    log_cfg = f'./cfg/log_{env}.json'       # logging config file
    upload_save = True                      # save uploaded file
    upload_clean = True                     # clean uploaded folder when startup
    cors_origins = [ 'http://localhost:8080', 'http://localhost:8100', 'http://192.168.11.160:8100' ]     # origins for CORS
    allowed_extensions = {'pdf', 'png', 'jpg', 'jpeg', 'gif', 'tif', 'tiff'}    # allowed upload extension
    max_content_length = 16 * 1024 * 1024   # 16MB max size for upload file
    # sqlalchemy_track_modifications = False
    # csrf_enable = True
    secret_key = 'K2elly_Education21'       # secret key
    jwt_algorithm = 'HS256'                 # JWT encryption algorithm for token
    jwt_expire_time = 1800                  # JWT access token expire time (in seconds)
    jwt_refresh_time = 1 * 24 * 3600        # JWT refresh token expire time (in seconds)
    sqlalchemy_echo = False                 # SQLAlchemy SQL debug flag
    sqlalchemy_database_uri = 'sqlite:///./instance/kellyed.db'  # database uri
    # sqlalchemy_database_uri = 'postgresql+psycopg2://kadmin:kelly2021@localhost/kellyed'
    # encrypted database uri for postgresql
    # sqlalchemy_database_uri = 'Z0FBQUFBQmdNNEJxek1TSS1HM2k5TnNRUEVUTmx5cXN3OFRrY3lKQnRHQkdvSEQ4eHZyUlVVeEVpeTRidXhCUzZ5SEgzRng4TmJGMWQ0RzZSUDdBMVR0dmY1VmdzWlU2QkNwWmhuR05aenk0NXBreUlaYUsyYTNDRHRrVEdHRElsZ0NQSmlvRVc3bkpDWDNnQjBBdDlwZi0xS3hvOTAyZVBRPT0='
    # mssql "Driver={SQL Server Native Client 11.0};Server=(LocalDB)\\MSSQLLocalDB;Database=kellyed;Integrated Security=true"
    # sqlalchemy_database_uri = "mssql+pyodbc:///?odbc_connect=Driver%3D%7BSQL%20Server%20Native%20Client%2011.0%7D%3BServer%3D%28LocalDB%29%5CMSSQLLocalDB%3BDatabase%3Dkellyed%3BIntegrated%20Security%3Dtrue"

    # excel sheets
    sheets = [
        'Choice', 'Staff', 'Student', 'Teacher', 'Product', 'Teacher Product', 
        'Course', 'Student Course', 'Lesson', 'Student Lesson', 'Sales Order', 'Payment'
    ]
    sample_xlsx = "./samples/import1.xlsx"

    # image utils settings
    image_prefix = 'p'
    image_format = 'jpg'
    thumb_name = '_t'
    thumb_size: List[int] = [140, 180]

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'

    # check and decrypt 'sqlalchemy_database_uri' string if needed
    @validator('sqlalchemy_database_uri', pre=True)
    def uri_check(cls, v, values):
        cipher_key = values.get('cipher_key', None)
        if cipher_key:
            from cryptography.fernet import Fernet
            from base64 import b64decode
            cipher = Fernet(cipher_key)
            try:
                v = cipher.decrypt(b64decode(v)).decode()
            except:
                pass
        # decrypted = cryptocode.decrypt(v, secret_key) if secret_key else None
        # return decrypted or v
        return v

settings = Settings()