from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from datetime import datetime, timedelta
from jose import jwt, JWTError, ExpiredSignatureError
from ..core.config import settings
from ..db import schemas
from ..core import utils

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/login")

async def get_token(token: str = Depends(oauth2_scheme), is_refresh=False):
    credentials_exception = utils.Unauthorized(
        detail="could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"}
    )
    if not token:
        raise credentials_exception
    secret = settings.secret_key
    if is_refresh:
        secret += '_r'
    algo = getattr(settings, "jwt_algorithm", 'HS256')
    payload = None
    try:
        payload = jwt.decode(token, secret, algorithms=[algo])
        # trust signed JWT payload
        # payload = schemas.UserBrief(**payload)
    except ExpiredSignatureError:
        credentials_exception.detail = "{} token expired".format("refresh" if is_refresh else "access")
        raise credentials_exception
    except JWTError as e:
        credentials_exception.detail = str(e)
        raise credentials_exception
    # invalid payload
    if not payload:
        raise credentials_exception
    return payload

async def get_payload(token: str = Depends(oauth2_scheme)):
    return await get_token(token=token, is_refresh=False)

async def get_refresh_payload(token: str = Depends(oauth2_scheme)):
    return await get_token(token=token, is_refresh=True)

def create_access_token(payload: dict, expire_seconds: int=0, is_refresh=False):
    if expire_seconds == 0:
        if is_refresh:
            expire_seconds = int(getattr(settings, 'jwt_refresh_time', 1 * 24 * 3600))
        else:
            expire_seconds = int(getattr(settings, 'jwt_expire_time', 1800))
    expire_time = datetime.utcnow() + timedelta(seconds=expire_seconds)
    # set expire time in encoded payload
    epayload = {**payload, "exp": expire_time}
    secret = settings.secret_key
    if is_refresh:
        secret += '_r'
    algo = getattr(settings, "jwt_algorithm", 'HS256')
    token = jwt.encode(epayload, secret, algorithm=algo)
    # convert to localtime timestamp in ms
    expire_seconds=int((datetime.now() + timedelta(seconds=expire_seconds)).timestamp() * 1000)
    return token, expire_seconds