from os import path, makedirs, unlink, scandir
from typing import List
from fastapi import Response, HTTPException, status, UploadFile
from .config import settings
import shutil
import json
import csv
from glob import glob
from uuid import uuid1
from hashlib import sha256
from base64 import b64decode, b64encode
# import cryptocode
from cryptography.fernet import Fernet
import logging

logger = logging.getLogger(__name__)
cipher = Fernet(settings.cipher_key)

class dict_obj(object):
    "for dict to object class"
    def __init__(self, d):
        for a, b in d.items():
            if isinstance(b, (list, tuple)):
               setattr(self, a, [dict_obj(x) if isinstance(x, dict) else x for x in b])
            else:
               setattr(self, a, dict_obj(b) if isinstance(b, dict) else b)

def str2bool(v: str) -> bool:
  return v.lower() in ("yes", "true", "t", "1")

def create_dirs(folder: str):
    if not path.exists(folder):
        # ensure the instance folder exists
        try:
            makedirs(folder)
        except OSError: # skip already exist exception
            pass

def instance_subfolder(*folders, is_makedirs=False):
    folder = path.join(settings.instance_path, *folders)
    if is_makedirs:
        if not path.exists(folder):
            makedirs(folder)
    return folder

def clear_folder(folder, is_delete=False):
    if not path.exists(folder):
        return
    if is_delete:
        # remove whole directory
        shutil.rmtree(folder, ignore_errors=True)
    else:
        # remove all sub-directory and files
        with scandir(folder) as it:
            for entry in it:
                if entry.is_dir():
                    shutil.rmtree(entry.path)
                else:
                    unlink(entry.path)

def load_csv(csv_file: str, delimiter=',', encoding='utf8'):
    records = []
    with open(csv_file, "rt", encoding=encoding) as fp:
        reader = csv.reader(fp, delimiter=delimiter)
        records = list(reader)
    return records

def load_json(json_file: str, to_obj=False):
    result = None
    with open(json_file, "rt", encoding='utf8') as fp:
        object_hook = dict_obj if to_obj else None
        result = json.load(fp, object_hook=object_hook)
    return result

def save_json(json_file: str, obj):
    with open(json_file, "wt", encoding='utf8') as fp:
        json.dump(obj, fp, indent=2)

def filter_dict(d: dict, columns: list):
    return {k: v for k, v in d.items() if k in columns}

def get_request_path(request):
    s = f"{request.method} {request.url.path}"
    if request.url.query:
        s += "?" + request.url.query
    return s

def OK():
    return Response(content="OK", status_code=status.HTTP_200_OK)

def NoContent():
    return Response(status_code=status.HTTP_204_NO_CONTENT)

def ItemCreated():
    return Response(status_code=status.HTTP_201_CREATED)

def RequestAccepted():
    return Response(status_code=status.HTTP_202_ACCEPTED)

def BadRequest(detail: str):
    return HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=detail)

def NotFound(detail: str):
    return HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=detail)

def Unauthorized(detail: str, headers=None):
    return HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=detail, headers=headers)

def InternalServerError(detail: str):
    return HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=detail)

async def save_upload_file(upload_file: UploadFile, save_file: str):
    try:
        with open(save_file, "wb") as fp:
            fp.write(await upload_file.read())
    finally:
        upload_file.file.close()

def allowed_file(filename):
    "Is allowed file to upload"
    return ('.' in filename and 
        filename.rsplit('.', 1)[1].lower() in settings.allowed_extensions)

async def get_uploaded_files(src_path: str, src_file: str, files: List[UploadFile], required=True):
    "get submitted files and save to temp upload folder"
    tmp_files = []
    # generate a temp unique path for this submission
    dst_name = '_T' + str(uuid1()).replace('-', '')
    dst_path = path.join(settings.upload_folder, dst_name)
    create_dirs(dst_path)
    if src_file:    # use server folder and filename for tmp_files
        if src_path:
            src_file = path.join(src_path, src_file)
        # allow use wildcard as filename to glob
        for f in glob(src_file, recursive=False):
            bf = path.basename(f)
            shutil.copy2(f, path.join(dst_path, bf))
            tmp_files.append(bf)
    elif files:         # use upload tmp_files
        # remove existing folder
        for rf in files: # key with multiple items
            if rf.filename == '':
                raise BadRequest("empty filename")
            if not allowed_file(rf.filename):
                raise BadRequest("not allowed image filename: {0}".format(rf.filename))
            # filename = secure_filename(rf.filename)
            filename = path.basename(rf.filename)
            save_path = path.join(dst_path, filename)
            await save_upload_file(upload_file=rf, save_file=save_path)
            tmp_files.append(filename)
    if required and len(tmp_files) == 0:
        raise BadRequest("no appropiated files provided")
    return dst_path, tmp_files

def tmp_filename(ext):
    # generate a temp unique path for this submission
    dst_name = '_T' + str(uuid1()).replace('-', '') + ext
    return path.join(settings.upload_folder, dst_name)

def hash_b64(text: str):
    return b64encode(sha256(text.encode('utf8')).digest()).decode('ascii')

# def encrypt_b64(text: str):
#     btext = text.encode('utf8')
#     salt = settings.secret_key.encode('utf8')
#     dk = pbkdf2_hmac('sha256', btext, salt, 100)
#     return b64encode(dk).decode('ascii')

def encrypt(text: str):
    # return cryptocode.encrypt(text, settings.secret_key)
    return b64encode(cipher.encrypt(text.encode())).decode('ascii')

def decrypt(text: str):
    # return cryptocode.decrypt(text, settings.secret_key)
    try:
        return cipher.decrypt(b64decode(text)).decode()
    except:
        return None

def fix_list_str(in_list, vtype=str, sep=','):
    return [vtype(x) for item in in_list for x in item.split(sep)]

def str_to_list(in_list, vtype=str, sep=','):
    if isinstance(in_list, str):
        return [vtype(x) for x in in_list.split(sep)]
    else:
        return in_list

def schema_dict(schema, sub_items={}):
    data = schema.dict(by_alias=True, exclude_unset=True, exclude=set(sub_items))
    for key, item_class in sub_items.items():
        if hasattr(schema, key):
            data[key] = [item_class(**x.dict()) for x in getattr(schema, key)]
    return data
