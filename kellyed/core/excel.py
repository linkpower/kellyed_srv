from typing import List
import sqlalchemy as sa
from .config import settings
from ..db import models
from ..db.session import Session
from datetime import datetime, date, time
import dateparser
from os import path, unlink
import openpyxl as xl
from openpyxl.styles import Alignment, Font
from .excel_config import sheet_info
import logging

logger = logging.getLogger(__name__)

def sort_sheets(sheets: List[str]):
    ordered = []
    for name in sheet_info.keys():
        if name in sheets:
            ordered.append(name)
    return ordered

def get_sheet_model(sheet):
    si = sheet_info.get(sheet, None)
    if not si:
        raise Exception(f"sheet not found: {sheet}")
    return si['model'], si.get('relationship', [])

def get_sheet_headers(sheet):
    si = sheet_info.get(sheet, None)
    if not si:
        raise Exception(f"sheet not found: {sheet}")
    return si['headers']

def open_xlsx(filename: str) -> xl.Workbook:
    return xl.load_workbook(filename)

def create_xlsx(sheets: List[str]) -> xl.Workbook:
    xlwb = xl.Workbook()
    for sheet in sheets:
        xlwb.create_sheet(sheet)
    # remove default 'Sheet'
    xlwb.remove_sheet(xlwb.worksheets[0])
    return xlwb

def save_xlsx(xlwb: xl.Workbook, filename: str):
    # if path.exists(filename):
    #     unlink(filename)
    xlwb.save(filename)

def close_xlsx(xlwb: xl.Workbook):
    xlwb.close()

def get_header_indexes(ws, si, add_missing=False):
    "get headers to excel column index"

    index_map = {}
    last_index = 0
    if ws.max_row > 0:
        # find excel header mapping to column index
        xl_headers = [x for row in ws.iter_rows(max_row=1) for x in row]
        for header in si['headers']:
            field = header['field']
            # skip for id using row number
            if header.get('row_number', False):
                index_map[field] = -1
                continue
            keyword = f"[{field}]"
            for i, xl_header in enumerate(xl_headers):
                if xl_header.value and xl_header.value.find(keyword) != -1:
                    index_map[field] = i
                    last_index = max(last_index, i + 1)
                    break
    # add missing index from worksheet headers
    if add_missing:
        for header in si['headers']:
            field = header['field']
            if index_map.get(field, None) is None:
                index_map[field] = last_index
                last_index += 1
    return index_map

def get_sheet_items(xlwb: xl.Workbook, sheet: str):
    si = sheet_info.get(sheet, None)
    ws = xlwb[sheet]
    if not si or not ws:
        raise Exception(f"sheet not found: {sheet}")
    maxrow = ws.max_row
    maxcol = ws.max_column
    if maxrow < 1 or maxcol < 1:
        raise Exception(f"empty rows or columns")
    # mapping headers to excel index
    index_map = get_header_indexes(ws, si)
    for i, row in enumerate(ws.iter_rows()):
        if i == 0:      # skip first row as header
            continue
        elif len(row) < 1 or row[0].value is None:  # skip empty tuple
            continue
        first_id = 0
        record = {}
        for name, col in index_map.items():
            record[name] = row[col].value if col >= 0 else i    # -1 for row number
            if col == 0:
                first_id = row[col].value
        yield record, first_id

def to_xlsx_value(value):
    if value is None:
        value = ''
    elif type(value) == bool:
        value = 1 if value else 0
    elif type(value) == date:
        value = datetime.strftime(value, '%Y-%m-%d')
    elif type(value) == datetime:
        value = datetime.strftime(value, '%Y-%m-%d %H:%M:%S')
    elif type(value) == time:
        value = time.strftime(value, '%H:%M:%S' if value.second > 0 else '%H:%M')
    else:
        value = getattr(value, 'value', str(value))
    return value

def from_xlsx_value(value, ctype):
    if value == None:
        return None
    elif type(ctype) == sa.Boolean:
        value = bool(value)
    elif type(ctype) == sa.Integer:
        value = int(value)
    elif type(ctype) == sa.Float:
        value = float(value)
    elif type(ctype) == sa.Date:
        dt = dateparser.parse(value) if type(value) == str else value
        value = dt.date() if dt else None
    elif type(ctype) == sa.DateTime:
        if type(value) == str:
            value = dateparser.parse(value)
    elif type(ctype) == sa.Time:
        if type(value) == str:
            value = dateparser.parse(value)
    else:
        # convert value for string
        value = to_xlsx_value(value)
    return value

def record_to_orm(db, sheet, record):
    si = sheet_info.get(sheet, None)
    if not si:
        raise Exception(f"sheet not found: {sheet}")
    model = si['model']
    headers = si['headers']
    key_field = si.get('key', None)
    orm = None
    # try to find existing record by primary key field
    if key_field:
        if isinstance(key_field, list):
            # find by multi-keys
            ids = {k: record[k] for k in key_field if k in record}
            orm = db.query(model).filter_by(**ids).one_or_none()
        elif key_field in record:
            # find by single key
            orm = db.query(model).get(record[key_field])
        # set modified time if have
        if orm and hasattr(orm, 'updated_at'):
            orm.updated_at = datetime.now()
    # new record if not found by primary key
    if not orm:
        orm = model()
    for header in headers:
        field = header['field']
        if field not in record:
            continue
        # skip for export only fields
        if header.get('export_only', False):
            continue
        value = record[field]
        to_list = header.get('to_list', None)
        # vtype = header.get('vtype', None)
        # add values as list of attribute
        # if to_list and vtype:
        if to_list:
            # split string by comma and make vtype model
            # items = [vtype(**{to_list: v.strip()}) for v in value.split(",")]
            # items = map(str.strip, value.split(","))
            if value:
                # convert to list with 'to_list' as type
                items = [to_list(v.strip()) for v in str(value).split(",")]
                setattr(orm, field, items)
        else:
            model_field = getattr(model, field)
            setattr(orm, field, from_xlsx_value(value, model_field.type))
    return orm

def update_sheet(xlwb: xl.Workbook, sheet: str, records: list, delete_all=True):
    si = sheet_info.get(sheet, None)
    if not si:
        raise Exception(f"sheet not found: {sheet}")
    # create sheet if not found
    if not sheet in xlwb.sheetnames:
        xlwb.create_sheet(sheet)
    ws = xlwb[sheet]
    # mapping headers to excel index
    index_map = get_header_indexes(ws, si, add_missing=True)
    # keep or modified headers
    for header in si['headers']:
        col = index_map.get(header['field'], None)
        # skip for non-field or row_number field
        if col is None or col == -1:
            continue
        c = ws.cell(row=1, column=col+1)
        if not c.value:
            # add new header title and style
            value = "{}\n[{}]".format(header['label'], header['field'])
            c.value = value
            c.font = Font(bold=True)
            c.alignment = Alignment(wrap_text=True, shrink_to_fit=True)
    # save record values
    last_row = 0
    for row_id, record in enumerate(records, start=2):
        last_row = row_id
        for header in si['headers']:
            value = getattr(record, header['field'])
            col = index_map.get(header['field'], None)
            # skip for non-field or row_number field
            if col is None or col == -1:
                continue
            if header.get('to_list', None):
                # value = ",".join([to_xlsx_value(getattr(x, header['to_list'])) for x in value])
                value = ",".join([to_xlsx_value(x) for x in value])
            else:
                value = to_xlsx_value(value)
            ws.cell(row=row_id, column=col+1, value=value)
    # delete remained records
    if delete_all and ws.max_row > last_row:
        ws.delete_rows(last_row + 1, ws.max_row - last_row)

def import_excel(db: Session, src_xslx: str, sheets: list, truncate: bool=False):
    total_imported = 0
    messages = []
    last_exception = ""
    xlwb = open_xlsx(src_xslx)
    for sheet in sheets:
        imported = 0
        failed = []
        model, relationship = get_sheet_model(sheet)
        # truncate tables if need
        if truncate:
            # delete all list for relationship first
            deleted = False
            for record in db.query(model).all():
                for rr in sa.inspect(model).relationships:
                    if rr.uselist:
                        val = getattr(record, rr.key)
                        if val:
                            setattr(record, rr.key, [])
                            deleted = True
            if deleted:
                db.commit()
            # truncate tables with relationship first
            for relative in relationship:
                rel_model = relative.get('model', None)
                filter_by = relative.get('filter_by', None)
                # delete by filtering or directly truncate
                if filter_by and rel_model:
                    db.query(rel_model).filter_by(**filter_by).delete()
                elif rel_model:
                    models.db_truncate(db, rel_model)
            # truncate actual table
            models.db_truncate(db, model)
            # models.db_recreate(model)
            db.commit()
        # get items from sheet
        for r, id in get_sheet_items(xlwb, sheet):
            try:
                # add/update record to orm object
                orm = record_to_orm(db, sheet, r)
                db.add(orm)
                db.commit()
                imported += 1
            except Exception as e:
                logger.warning(f"Excel Import Failed: {e}")
                last_exception = str(e)
                db.rollback()
                failed.append(id)
        total_imported += imported
        logger.info(f"import_excel: {sheet}, imported {imported}")
        messages.append(f"{sheet} imported successfully: {imported}")
        if len(failed) > 0:
            logger.warning(f"import_excel failed: {sheet}, failed {failed}")
            messages.append(f"{sheet} import failed: {failed}")
            messages.append(f"{sheet} exception: {last_exception}")

    return total_imported, messages

def export_excel(db: Session, sheets: list, dst_xlsx:str, src_xlsx: str=None):
    if src_xlsx:
        xlwb = open_xlsx(src_xlsx)
    else:
        xlwb = create_xlsx(sheets=sheets)
    for sheet in sheets:
        model, _ = get_sheet_model(sheet=sheet)
        records = db.query(model).all()
        update_sheet(xlwb, sheet, records)
    save_xlsx(xlwb, dst_xlsx)