from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object
crud = crud.BaseCrud(name='product', model=models.Product)

def filter_keyword(dbo, keyword=None, names_only=False):
    if keyword:
        like_str = f"%{keyword}%"
        filter_list = models.or_(
            models.Product.name.ilike(like_str),
            models.Product.product_code.ilike(like_str),
        )
        # more fields search when not names only
        if not names_only:
            filter_list += models.or_(
                models.Product.category.ilike(like_str),
                models.Product.language.ilike(like_str),
                models.Product.formation.ilike(like_str),
                models.Product.level.ilike(like_str),
            )
        dbo = dbo.filter(filter_list)
    return dbo

def filter_category(dbo, category):
    categories = utils.str_to_list(category)
    return dbo.filter(models.Product.category.in_(categories))

@router.get("", response_model=List[schemas.Product])
async def list_product(
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    names_only: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword, names_only=names_only)

@router.get("/brief", response_model=List[schemas.ProductBrief])
async def list_product_brief(
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    names_only: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword, names_only=names_only)

@router.get("/count")
async def count_product(keyword: str=None, db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword)
    return {'count': count}

@router.get("/category/{category}", response_model=List[schemas.Product])
async def list_product_category(category: str, db: Session = Depends(get_db)):
    return crud.list(db, filter_func=filter_category, category=category)

@router.get("/{product_id}", response_model=schemas.Product, response_model_exclude_none=True)
async def get_product(product_id: int, db: Session = Depends(get_db)):
    product = crud.read(db, id=product_id)
    if not product:
        raise utils.NotFound("product not found")
    return product

@router.post("", response_model=schemas.Product)
async def create_product(item: schemas.ProductCreate, db: Session = Depends(get_db)):
    product = crud.create(db, item=item)
    return product

@router.put("/{product_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_product(product_id: int, item: schemas.ProductUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=product_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{product_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_product(product_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=product_id)
    return utils.RequestAccepted()
