from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object, and 'roles' is a sub_items in Staff
crud = crud.BaseCrud(name='staff', model=models.Staff, 
    sub_items={'roles': models.UserRole})

def filter_keyword(dbo, keyword=None, roles=None, names_only=False):
    if keyword:
        like_str = f"%{keyword}%"
        filter_list = models.or_(
            models.Staff.name.ilike(like_str),
            models.Staff.staff_code.ilike(like_str),
        )
        if not names_only:
            filter_list += models.or_(
                models.Staff.initial.ilike(like_str),
                models.Staff.email.ilike(like_str),
                models.Staff.mobile.ilike(like_str),
            )
        dbo = dbo.filter(filter_list)
    if roles:
        roles = utils.str_to_list(roles)
        dbo = dbo.join(models.Staff.roles).filter(models.UserRole.role.in_(roles))
    return dbo

def filter_email(dbo, email):
    return dbo.filter_by(email=email)

@router.get("", response_model=List[schemas.Staff])
async def list_staff(
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    names_only: bool=False,
    roles: str=None,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword, names_only=names_only, roles=roles)

@router.get("/count")
async def count_staff(keyword: str=None, roles: str=None, 
    db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword, roles=roles)
    return {'count': count}

@router.get("/{user_id}", response_model=schemas.Staff, response_model_exclude_none=True)
async def get_staff(user_id: int, db: Session = Depends(get_db)):
    staff = crud.read(db, id=user_id)
    if not staff:
        raise utils.NotFound("staff not found")
    return staff

@router.post("", response_model=schemas.Staff)
async def create_staff(item: schemas.StaffCreate, db: Session = Depends(get_db)):
    old_staff = crud.read(db, email=item.email)
    if old_staff:
        raise utils.BadRequest(f"Email '{item.email}' is already existed")
    if not item.roles or len(item.roles) == 0:
        raise utils.BadRequest(f"Staff is missing role(s)")
    staff = crud.create(db, item=item)
    return staff

@router.put("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_staff(user_id: int, item: schemas.StaffUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=user_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_staff(user_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=user_id)
    return utils.RequestAccepted()
