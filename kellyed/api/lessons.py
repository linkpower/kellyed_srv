from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
from datetime import date, timedelta
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object, and 'roles' is a sub_items in Lesson
crud = crud.BaseCrud(name='lesson', model=models.Lesson, 
    sub_items={'student_actions': models.StudentLesson},
    join_models=[models.Course])

def filter_keyword(dbo, keyword=None, date_field=None, date_start=0, date_end=0):
    if keyword:
        like_str = f"%{keyword}%"
        # filter lesson by course name or course code by joining course
        return dbo.filter(models.or_(
                models.Course.name.ilike(like_str),
                models.Course.course_code.ilike(like_str),
            )
        )
    if date_field:
        if date_start > 0:
            d = date.fromtimestamp(date_start)
            dbo = dbo.filter(models.Lesson.schedule_start >= d)
        if date_end > 0:
            d = date.fromtimestamp(date_end) + timedelta(days=1)
            dbo = dbo.filter(models.Lesson.schedule_end < d)
    return dbo

@router.get("", response_model=List[schemas.Lesson])
async def list_lesson(
    keyword: str=None,
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end, join=True)

@router.get("/count")
async def count_lesson(
    keyword: str=None, 
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end, join=True)
    return {'count': count}

@router.get("/{lesson_id}", response_model=schemas.LessonDetail, response_model_exclude_none=True)
async def get_lesson(lesson_id: int, db: Session = Depends(get_db)):
    lesson = crud.read(db, id=lesson_id)
    if not lesson:
        raise utils.NotFound("lesson not found")
    return lesson

@router.post("", response_model=schemas.Lesson)
async def create_lesson(item: schemas.LessonCreate, db: Session = Depends(get_db)):
    lesson = crud.create(db, item=item)
    return lesson

@router.put("/{lesson_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_lesson(lesson_id: int, item: schemas.LessonUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=lesson_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{lesson_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_lesson(lesson_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=lesson_id)
    return utils.RequestAccepted()
