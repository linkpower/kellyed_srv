from fastapi import APIRouter, Depends
from typing import List
from ..core.config import settings
from ..db import schemas, models, crud
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object
crud = crud.BaseCrud(name='choice', model=models.Choice)

def filter_keyword(dbo, keyword):
    if keyword:
        like_str = f"%{keyword}%"
        dbo = dbo.filter(models.or_(
            models.Choice.category.ilike(like_str),
            models.Choice.text.ilike(like_str),
            models.Choice.value.ilike(like_str),
        ))
    return dbo

def filter_category(dbo, category):
    return dbo.filter_by(category=category)

@router.get("", response_model=List[schemas.Choice])
async def list_choice(
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    # return crud.list_choice(db, keyword=keyword, offset=offset, limit=page_size, 
    #     order_by=sort_by, order_desc=sort_desc)
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword)

@router.get("/count")
async def count_choice(keyword: str=None, db: Session = Depends(get_db)):
    # count = crud.count_choice(db, keyword=keyword)
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword)
    return {'count': count}

@router.get("/excel_sheets")
async def list_excel_sheets(db: Session = Depends(get_db)):
    return [{'text': sheet, 'value': sheet} for sheet in settings.sheets]

@router.get("/{category}", response_model=List[schemas.Choice])
async def list_choice_category(category: str, db: Session = Depends(get_db)):
    # return crud.list_choice_category(db, category)
    return crud.list(db, filter_func=filter_category, category=category)

# @router.get("/{choice_id}", response_model=schemas.Choice, response_model_exclude_none=True)
# async def get_choice(choice_id: int, db: Session = Depends(get_db)):
#     choice = crud.read_choice(db, choice_id=choice_id)
#     if not choice:
#         raise utils.NotFound("choice not found")
#     return choice

# @router.post("", response_model=schemas.Choice)
# async def create_choice(item: schemas.ChoiceCreate, db: Session = Depends(get_db)):
#     choice = crud.create_choice(db, choice=item)
#     return choice

# @router.put("/{choice_id}", status_code=status.HTTP_202_ACCEPTED)
# async def update_choice(choice_id: int, item: schemas.Choice, db: Session = Depends(get_db)):
#     crud.update_choice(db, choice_id=choice_id, choice=item)
#     return utils.RequestAccepted()

# @router.delete("/{choice_id}", status_code=status.HTTP_202_ACCEPTED)
# async def delete_choice(choice_id: int, db: Session = Depends(get_db)):
#     crud.delete_choice(db, choice_id=choice_id)
#     return utils.RequestAccepted()
