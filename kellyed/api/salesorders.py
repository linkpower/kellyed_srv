from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
from datetime import date, timedelta
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object
crud = crud.BaseCrud(name='sales order', model=models.SalesOrder, 
    join_models=[
        models.Course,
        [models.SalesOrder._student, models.SalesOrder.student_id == models.SalesOrder._student.user_id],
        [models.SalesOrder._sales, models.SalesOrder.sales_id == models.SalesOrder._sales.user_id],
    ])

def filter_keyword(dbo, keyword=None, date_field=None, date_start=0, date_end=0):
    if keyword:
        like_str = f"%{keyword}%"
        dbo = dbo.filter(models.or_(
                models.SalesOrder.so_number.ilike(like_str),
                models.SalesOrder.student_name.ilike(like_str),
                models.SalesOrder.sales_name.ilike(like_str),
            )
        )
    if date_field:
        if date_start > 0:
            d = date.fromtimestamp(date_start)
            dbo = dbo.filter(models.SalesOrder.so_date >= d)
        if date_end > 0:
            d = date.fromtimestamp(date_end) + timedelta(days=1)
            dbo = dbo.filter(models.SalesOrder.so_date < d)
    return dbo

@router.get("", response_model=List[schemas.SalesOrder])
async def list_salesorder(
    keyword: str=None,
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    brief: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end, join=not brief)

@router.get("/count")
async def count_salesorder(
    keyword: str=None, 
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end, join=True)
    return {'count': count}

@router.get("/{so_id}", response_model=schemas.SalesOrderDetail, response_model_exclude_none=True)
async def get_salesorder(so_id: int, db: Session = Depends(get_db)):
    salesorder = crud.read(db, id=so_id)
    if not salesorder:
        raise utils.NotFound("salesorder not found")
    return salesorder

@router.post("", response_model=schemas.SalesOrder)
async def create_salesorder(item: schemas.SalesOrderCreate, db: Session = Depends(get_db)):
    salesorder = crud.create(db, item=item)
    return salesorder

@router.put("/{so_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_salesorder(so_id: int, item: schemas.SalesOrderUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=so_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{so_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_salesorder(so_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=so_id)
    return utils.RequestAccepted()
