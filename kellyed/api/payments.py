from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
from datetime import date, timedelta
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object
crud = crud.BaseCrud(name='payment', model=models.Payment,
    join_models=[
        models.SalesOrder,
        [models.User, models.Payment.received_by == models.User.user_id],
    ])

def filter_keyword(dbo, keyword=None, date_field=None, date_start=0, date_end=0):
    if keyword:
        like_str = f"%{keyword}%"
        dbo = dbo.filter(models.or_(
                models.Payment.payee_name.ilike(like_str),
                models.Payment.payee_ref.ilike(like_str),
                models.Payment.account_code.ilike(like_str),
                models.Payment.so_number.ilike(like_str),
            )
        )
    if date_field:
        if date_start > 0:
            d = date.fromtimestamp(date_start)
            dbo = dbo.filter(models.SalesOrder.so_date >= d)
        if date_end > 0:
            d = date.fromtimestamp(date_end) + timedelta(days=1)
            dbo = dbo.filter(models.SalesOrder.so_date < d)
    return dbo

@router.get("", response_model=List[schemas.Payment])
async def list_payment(
    keyword: str=None,
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end, join=True)

@router.get("/count")
async def count_payment(
    keyword: str=None, 
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end, join=True)
    return {'count': count}

@router.get("/{payment_id}", response_model=schemas.PaymentDetail, response_model_exclude_none=True)
async def get_payment(payment_id: int, db: Session = Depends(get_db)):
    payment = crud.read(db, id=payment_id)
    if not payment:
        raise utils.NotFound("payment not found")
    return payment

@router.post("", response_model=schemas.Payment)
async def create_payment(item: schemas.PaymentCreate, db: Session = Depends(get_db)):
    payment = crud.create(db, item=item)
    return payment

@router.put("/{payment_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_payment(payment_id: int, item: schemas.PaymentUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=payment_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{payment_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_payment(payment_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=payment_id)
    return utils.RequestAccepted()
