from fastapi import APIRouter, Depends, Form, UploadFile, File
from fastapi.responses import StreamingResponse
from typing import List
from io import BytesIO
from os import path, unlink
from ..core import utils, excel
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

@router.post("/import")
async def import_excel(
    sheets: List[str] = Form(...), 
    truncate: bool = Form(False), 
    file: UploadFile = File(...),
    db: Session = Depends(get_db)):
    "import records from xlsx file"

    sheets = utils.fix_list_str(sheets)
    logger.info(f"import_excel: {sheets}, {file.filename}")
    sheets = excel.sort_sheets(sheets)
    tmp_file = utils.tmp_filename('.xlsx')
    total_imported = 0
    messages = []
    try:
        await utils.save_upload_file(file, tmp_file)
        total_imported, messages = excel.import_excel(db=db, 
            src_xslx=tmp_file, sheets=sheets, truncate=truncate)
    finally:
        if path.exists(tmp_file):
            unlink(tmp_file)
    return {
        'imported': total_imported,
        'messages': messages
    }

@router.post("/export", response_class=StreamingResponse)
async def export_excel(
    filename: str = Form('export_kellyed.xlsx'),
    sheets: List[str] = Form(...), 
    file: UploadFile = File(None),
    db: Session = Depends(get_db)):
    "export records to xlsx file"

    sheets = utils.fix_list_str(sheets)
    logger.debug(f"export_excel: {sheets}")
    # get all data from sheet model
    tmp_file = utils.tmp_filename('.xlsx')
    if file:
        await utils.save_upload_file(file, tmp_file)
        src_file = tmp_file
    else:
        src_file = None
    byte_data = b''
    try:
        excel.export_excel(db, sheets=sheets, dst_xlsx=tmp_file, src_xlsx=src_file)
        with open(tmp_file, 'rb') as tmp:
            byte_data = tmp.read()
    finally:
        if path.exists(tmp_file):
            unlink(tmp_file)
    logger.info(f"export_excel: {sheets}, {filename}")
    media_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    headers = { 'content-disposition': f'attachment; filename="{filename}"' }

    return StreamingResponse(BytesIO(byte_data), headers=headers, media_type=media_type)

# @router.get("/count")
# async def count_choice(options: schemas.SearchOptions=None, db: Session = Depends(get_db)):
#     count = crud.count_choice(db)
#     return {'count': count}

# @router.get("/{choice_id}", response_model=schemas.Choice, response_model_exclude_none=True)
# async def get_choice(choice_id: int, db: Session = Depends(get_db)):
#     choice = crud.read_choice(db, choice_id=choice_id)
#     if not choice:
#         raise utils.NotFound("choice not found")
#     return choice

# @router.post("", response_model=schemas.Choice)
# async def create_choice(item: schemas.ChoiceCreate, db: Session = Depends(get_db)):
#     choice = crud.create_choice(db, choice=item)
#     return choice

# @router.put("/{choice_id}", status_code=status.HTTP_202_ACCEPTED)
# async def update_choice(choice_id: int, item: schemas.Choice, db: Session = Depends(get_db)):
#     crud.update_choice(db, choice_id=choice_id, choice=item)
#     return utils.RequestAccepted()

# @router.delete("/{choice_id}", status_code=status.HTTP_202_ACCEPTED)
# async def delete_choice(choice_id: int, db: Session = Depends(get_db)):
#     crud.delete_choice(db, choice_id=choice_id)
#     return utils.RequestAccepted()
