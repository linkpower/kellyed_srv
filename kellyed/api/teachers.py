from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object, and 'roles' is a sub_items in Teacher
crud = crud.BaseCrud(name='teacher', model=models.Teacher, 
    sub_items={'roles': models.UserRole})

def filter_keyword(dbo, keyword=None, names_only=False):
    if keyword:
        like_str = f"%{keyword}%"
        filter_list = models.or_(
            models.Teacher.name.ilike(like_str),
            models.Teacher.teacher_code.ilike(like_str),
        )
        if not names_only:
            filter_list += models.or_(
                models.Teacher.initial.ilike(like_str),
                models.Teacher.email.ilike(like_str),
                models.Teacher.mobile.ilike(like_str),
            )
        dbo = dbo.filter(filter_list)
    return dbo

def filter_email(dbo, email):
    return dbo.filter_by(email=email)

@router.get("", response_model=List[schemas.Teacher])
async def list_teacher(
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    names_only: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword, names_only=names_only)

@router.get("/count")
async def count_teacher(keyword: str=None, db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword)
    return {'count': count}

@router.get("/{user_id}", response_model=schemas.TeacherDetail, response_model_exclude_none=True)
async def get_teacher(user_id: int, db: Session = Depends(get_db)):
    teacher = crud.read(db, id=user_id)
    if not teacher:
        raise utils.NotFound("teacher not found")
    return teacher

@router.post("", response_model=schemas.TeacherDetail)
async def create_teacher(item: schemas.TeacherCreate, db: Session = Depends(get_db)):
    old_teacher = crud.read(db, email=item.email)
    if old_teacher:
        raise utils.BadRequest(f"Email '{item.email}' is already existed")
    item.roles = [schemas.UserRole(role='Teacher')]    # set student role
    teacher = crud.create(db, item=item)
    return teacher

@router.put("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_teacher(user_id: int, item: schemas.TeacherUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=user_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_teacher(user_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=user_id)
    return utils.RequestAccepted()
