from fastapi import APIRouter, HTTPException, status, WebSocket, WebSocketDisconnect
from typing import List
import logging

logger = logging.getLogger(__name__)

router = APIRouter()

class ConnectionManager:
    app = None

    def __init__(self):
        self.active_connections: List[WebSocket] = []

    def set_app(self, app):
        self.app = app

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def broadcast(self, message, websocket: WebSocket=None):
        for connection in self.active_connections:
            if connection is not websocket:
                await connection.send_json(message)
    
    async def receive_data(self, websocket: WebSocket):
        data = await websocket.receive_json()
        logger.debug(f"ws recv: {data}")

manager = ConnectionManager()

@router.websocket("")
async def ws_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    try:
        while True:
            await websocket.receive_json()
            # await websocket.send_text(f"Message text was: {data}")
    except WebSocketDisconnect:
        manager.disconnect(websocket)