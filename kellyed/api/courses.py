from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
from datetime import date, timedelta
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object
crud = crud.BaseCrud(name='course', model=models.Course)

def filter_keyword(dbo, keyword=None, date_field=None, date_start=0, date_end=0):
    if keyword:
        like_str = f"%{keyword}%"
        dbo = dbo.filter(
            models.or_(
                models.Course.name.ilike(like_str),
                models.Course.course_code.ilike(like_str),
            )
        )
    if date_field:
        if date_start > 0:
            d = date.fromtimestamp(date_start)
            dbo = dbo.filter(models.Course.lesson_first_date >= d)
        if date_end > 0:
            d = date.fromtimestamp(date_end) + timedelta(days=1)
            dbo = dbo.filter(models.Course.lesson_first_date < d)
    return dbo

@router.get("", response_model=List[schemas.Course])
async def list_course(
    keyword: str=None,
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end)

@router.get("/count")
async def count_course(
    keyword: str=None, 
    date_field: str=None, 
    date_start: int=0, 
    date_end: int=0, 
    db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword, 
        date_field=date_field, date_start=date_start, date_end=date_end)
    return {'count': count}

@router.get("/{course_id}", response_model=schemas.CourseDetail, response_model_exclude_none=True)
async def get_course(course_id: int, db: Session = Depends(get_db)):
    course = crud.read(db, id=course_id)
    if not course:
        raise utils.NotFound("course not found")
    return course

@router.post("", response_model=schemas.Course)
async def create_course(item: schemas.CourseCreate, db: Session = Depends(get_db)):
    course = crud.create(db, item=item)
    return course

@router.put("/{course_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_course(course_id: int, item: schemas.CourseUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=course_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{course_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_course(course_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=course_id)
    return utils.RequestAccepted()
