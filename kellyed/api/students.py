from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()

# make a crud object, and 'roles' is a sub_items in Student
crud = crud.BaseCrud(name='student', model=models.Student, 
    sub_items={'roles': models.UserRole})

def filter_keyword(dbo, keyword=None, ids=None, names_only=False):
    if keyword:
        like_str = f"%{keyword}%"
        filter_list = models.or_(
            models.Student.name.ilike(like_str),
            models.Student.student_code.ilike(like_str),
        )
        if not names_only:
            filter_list += models.or_(
                models.Student.initial.ilike(like_str),
                models.Student.email.ilike(like_str),
                models.Student.mobile.ilike(like_str),
                models.Student.trial_student_code.ilike(like_str),
            )
        dbo = dbo.filter(filter_list)
    if ids:
        ids = utils.str_to_list(ids)
        dbo = dbo.filter(models.Student.user_id.in_(ids))
    return dbo

def filter_email(dbo, email):
    return dbo.filter_by(email=email)

@router.get("", response_model=List[schemas.Student])
async def list_student(
    ids: str=None,
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    names_only: bool=False,
    db: Session = Depends(get_db)):
    "list student"
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword, names_only=names_only, ids=ids)

@router.get("/count")
async def count_student(keyword: str=None, db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword)
    return {'count': count}

@router.get("/{user_id}", response_model=schemas.StudentDetail, response_model_exclude_none=True)
async def get_student(user_id: int, db: Session = Depends(get_db)):
    student = crud.read(db, id=user_id)
    if not student:
        raise utils.NotFound("student not found")
    return student

@router.post("", response_model=schemas.StudentDetail)
async def create_student(item: schemas.StudentCreate, db: Session = Depends(get_db)):
    old_student = crud.read(db, email=item.email)
    if old_student:
        raise utils.BadRequest(f"Email '{item.email}' is already existed")
    item.roles = [schemas.UserRole(role='Student')]    # set student role
    student = crud.create(db, item=item)
    return student

@router.put("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_student(user_id: int, item: schemas.StudentUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=user_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_student(user_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=user_id)
    return utils.RequestAccepted()
