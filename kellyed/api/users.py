from fastapi import APIRouter, Depends, status
from typing import List
from ..core import utils
from ..db import schemas, models, crud
from ..db.session import Session, get_db
import logging

logger = logging.getLogger(__name__)
router = APIRouter()
# make a crud object, and 'roles' is a sub_items in User
crud = crud.BaseCrud(name='user', model=models.User, 
    sub_items={'roles': models.UserRole})

def filter_keyword(dbo, keyword=None, names_only=False):
    if keyword:
        like_str = f"%{keyword}%"
        filter_list = models.or_(
            models.User.name.ilike(like_str),
        )
        if not names_only:
            filter_list += models.or_(
                models.User.initial.ilike(like_str),
                models.User.email.ilike(like_str),
                models.User.mobile.ilike(like_str),
            )
        dbo = dbo.filter(filter_list)
    return dbo

def filter_email(dbo, email):
    return dbo.filter_by(email=email)

@router.get("", response_model=List[schemas.User])
async def list_user(
    keyword: str=None,
    page: int=0,
    page_size: int=10,
    sort_by: str=None,
    sort_desc: bool=False,
    db: Session = Depends(get_db)):
    offset = page * page_size
    return crud.list(db, offset=offset, limit=page_size, 
        order_by=sort_by, order_desc=sort_desc, 
        filter_func=filter_keyword, keyword=keyword)

@router.get("/count")
async def count_user(keyword: str=None, db: Session = Depends(get_db)):
    count = crud.count(db, filter_func=filter_keyword, keyword=keyword)
    return {'count': count}

@router.get("/{user_id}", response_model=schemas.User, response_model_exclude_none=True)
async def get_user(user_id: int, db: Session = Depends(get_db)):
    user = crud.read(db, id=user_id)
    if not user:
        raise utils.NotFound("user not found")
    return user

@router.post("", response_model=schemas.User)
async def create_user(item: schemas.UserCreate, db: Session = Depends(get_db)):
    old_user = crud.read(db, email=item.email)
    if old_user:
        raise utils.BadRequest(f"Email '{item.email}' is already existed")
    if not item.roles or len(item.roles) == 0:
        raise utils.BadRequest(f"User is missing role(s)")
    user = crud.create(db, item=item)
    return user

@router.put("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def update_user(user_id: int, item: schemas.UserUpdate, db: Session = Depends(get_db)):
    crud.update(db, id=user_id, item=item)
    return utils.RequestAccepted()

@router.delete("/{user_id}", status_code=status.HTTP_202_ACCEPTED)
async def delete_user(user_id: int, db: Session = Depends(get_db)):
    crud.delete(db, id=user_id)
    return utils.RequestAccepted()
