from os import path
from fastapi import APIRouter, HTTPException, status, Request, Query
from fastapi.responses import PlainTextResponse, RedirectResponse, FileResponse
from ..core import utils
from ..core.config import settings

router = APIRouter()

@router.get("/status", summary="Get Server Status", response_class=PlainTextResponse)
async def get_status():
    return "OK"

@router.get("/file/{folder}/{filename}", response_class=FileResponse)
async def get_file(folder: int, filename: str, _t: str = Query(None)):
    out_file = filename
    image_file = path.join(settings.batch_folder, str(folder), out_file)
    if not path.exists(image_file):
        raise utils.NotFound("image file not exist")
    return FileResponse(image_file)