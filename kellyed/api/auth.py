from fastapi import APIRouter, Depends, Form
from fastapi.security import OAuth2PasswordRequestForm
from ..db import schemas, models, crud
from ..db.session import Session, get_db
from ..core import utils, oauth2

# client javascript:
# // Hash function in javascript
# function hash(algo, str) {
#   return crypto.subtle.digest(algo, new TextEncoder().encode(str));
# }
#
# // encode base64 from array buffer by hash function
# function encode64(buff) {
#   return btoa(String.fromCharCode.apply(null, new Uint8Array(buff)));
# }
#
# // use hash function and encode function to generate a hashed password
# const hpasswd = encode64(await hash('SHA-256', 'Hello'));
# // or, 
# hash('SHA-256', 'Hello').then(hashed => {
#   console.log(hashed); // ArrayBuffer
#   console.log(encode64(hashed)); // GF+NsyJx/iX1Yab8k4suJkMG7DBO2lGAB9F2SCY4GWk=
# });
#
# // login server API for oauth2 authentication
# data = {
#     "username": "admin",
#     "password": "jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=",
#     "grant_type": "password"
# }
# token = await axios.post('/api/auth/login', data=data)
#
# // add header for server API that required oauth2 authentication
# current_user = await axios.get('/api/auth/current_user', headers={
#     "Authorization": token['token_type'] + ' ' + token['access_token']
# })

router = APIRouter()

# make a crud object
crud = crud.BaseCrud(name='user', model=models.User)

# server side hash string function
@router.post("/hash_str", summary="Hash String Text")
async def get_hash_str(text: str = Form(...)):
    return { "hashed": utils.hash_b64(text) }

@router.post("/encrypt_str", summary="Encrypt String Text")
async def get_encrypt_str(text: str = Form(...)):
    return { "encrypted": utils.encrypt(text) }

@router.post("/login", response_model=schemas.AccessToken)
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    "login to get access token and refresh token"
    username = form_data.username
    password = form_data.password
    user = crud.read(db, email=username)
    # hash plain password if not hashed
    if len(password) < 11:
        password = utils.hash_b64(password)
    if not user or utils.hash_b64(user.password) != password:
        raise utils.Unauthorized("Incorrect username or password")
    # payload = schemas.UserBrief.from_orm(user).dict()
    payload = {'username': username}
    # create access and refresh tokens
    access_token, access_expire = oauth2.create_access_token(payload)
    refresh_token, refresh_expire = oauth2.create_access_token(payload, is_refresh=True)
    return schemas.AccessToken(
        token_type='Bearer',
        access_token=access_token,
        refresh_token=refresh_token,
        expire_time=access_expire
    )

@router.get("/refresh")
async def refresh(payload = Depends(oauth2.get_refresh_payload)):
    "refresh new access token"
    # payload = schemas.UserBrief.from_orm(user).dict()
    new_payload = {'username': payload['username']}
    access_token, access_expire = oauth2.create_access_token(new_payload)
    return {
        'access_token': access_token,
        'expire_time': access_expire
    }

@router.get("/current_user", response_model=schemas.User)
async def current_user(payload = Depends(oauth2.get_payload), db: Session = Depends(get_db)):
    "get current user by access token"
    user = crud.read(db, email=payload['username'])
    return user

# @router.get("/logout")
# async def logout(user: schemas.UserBrief = Depends(oauth2.get_payload)):
#     return utils.OK()
