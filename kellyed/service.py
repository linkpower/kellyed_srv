from os import path, chdir
project_path = path.dirname(path.dirname(path.realpath(__file__)))
chdir(project_path)

import sys
sys.path.append(".")            # include current path for lookup module

import win32serviceutil
import win32service
import win32event
import servicemanager
import socket
import logging
import uvicorn
from kellyed.core.config import settings
from kellyed.core import utils
from kellyed.main import app

logger = logging.getLogger(__name__)

class AppServerSvc(win32serviceutil.ServiceFramework):
    _svc_name_ = "KellyEdSrv"
    _svc_display_name_ = "KellyEd Web Service"

    def __init__(self, args):
        win32serviceutil.ServiceFramework.__init__(self, args)
        self.hWaitStop = win32event.CreateEvent(None,0,0,None)
        socket.setdefaulttimeout(5)
        self.stop_requested = False

    def SvcStop(self):
        self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
        win32event.SetEvent(self.hWaitStop)
        self.ReportServiceStatus(win32service.SERVICE_STOPPED)
        logger.info('KellyEdSrv Stopped')
        self.stop_requested = True

    def SvcDoRun(self):
        servicemanager.LogMsg(servicemanager.EVENTLOG_INFORMATION_TYPE,
                              servicemanager.PYS_SERVICE_STARTED,
                              (self._svc_name_,''))
        # self.ReportServiceStatus(win32service.SERVICE_START_PENDING) 
        main()
        # stop service
        rc = None  
        # if the stop event hasn't been fired keep looping  
        while rc != win32event.WAIT_OBJECT_0:  
            # block for 2 seconds and listen for a stop event  
            rc = win32event.WaitForSingleObject(self.hWaitStop, 2000)
        self.stop()

def main():
    # workaround for uvicorn init logging
    logger_config = utils.load_json(settings.log_cfg)
    config = uvicorn.Config(app, host=settings.listen_host, 
        reload=False, port=settings.listen_port, log_config=logger_config)
    server = uvicorn.Server(config=config)
    # workaround for signal only works in main thread
    server.install_signal_handlers = lambda: None
    server.run()

def run_service():
    win32serviceutil.HandleCommandLine(AppServerSvc)

if __name__ == "__main__":
    # run production server in windows service
    run_service()
