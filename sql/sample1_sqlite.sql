DELETE FROM sqlite_sequence;
DELETE FROM student;
DELETE FROM user;

INSERT INTO "user" VALUES (1,'staff','Admin','AD','admin','admin@a.com','12345678',false,'["Admin"]','2020-08-10 18:21:02.097972','2020-08-10 18:21:02.387031');
-- INSERT INTO "user_role" VALUES (1, 1, 'Admin');
INSERT INTO "user" VALUES (2,'student','ST1','AD','admin','st1@a.com','12345678',false,'["Student"]','2020-08-10 18:21:02.097972','2020-08-10 18:21:02.387031');
-- INSERT INTO "user_role" VALUES (2, 2, 'Student');
INSERT INTO "user" VALUES (3,'student','ST2','AD','admin','st2@a.com','12345678',false,'["Student"]','2020-08-10 18:21:02.097972','2020-08-10 18:21:02.387031');
-- INSERT INTO "user_role" VALUES (3, 3, 'Student');

INSERT INTO "staff" VALUES (1,'AD1','Male');
INSERT INTO "student" VALUES (2,'STC1','TSOW1','RC1','Peter Chan','David Chan','May Lee','12345678','91234567','2010-10-14','Male');
INSERT INTO "student" VALUES (3,'STC2','TSOW2','RC2','Peter Chan','David Chan','May Lee','22345678','92234567','2010-11-24','Male');


INSERT INTO "choice" VALUES (1,'gender','Male','"Male"');
INSERT INTO "choice" VALUES (2,'gender','Female','"Female"');
INSERT INTO "choice" VALUES (3,'gender','NA','"NA"');