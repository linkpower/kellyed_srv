# KellyEd project web application server

## Project Link
[Frontend UI](http://localhost:8100/static/)
[Base API path](http://localhost:8100/api/)
[OpenAPI Doc Link](http://localhost:8100/api/docs)
[ReDoc Link](http://localhost:8100/api/redoc)

## VS Code Setup
* Update to latest VS Code (at least Version 1.48)
* Update to latest Python extension
* install "pylance" language server for better Python intellisense

## Server Setup
* install python package
```
pip install -r requirements.txt
```
* [Optional] install pywin32 package for windows service
```
pip install pywin32
```
* [Optional] run post-install script for pywin32 for windows service:
```
python "C:\Program Files\Python37\Scripts\pywin32_postinstall.py" -install
```
* server initial setup (with delete all db tables) and create super user
```
python manage.py -s -d -c
```
* [Optional] install windows service for production by Administrator
```
python kellyed_srv/service.py --startup auto install
```
* [Optional] restart windows service
```
python main.py restart
```

## Server Uninstall
* uninstall KellyEd service by Administrator
```
python kellyed_srv/service.py remove
```

## Config Files
* settings are by environment variables, refer to kellyed_srv/core/config.py
* development log config - cfg/log_development.json
* production log config - cfg/log_production.json
* testing log config - cfg/log_testing.json

## Database Manager (Optional)
* upgrade upgrade database schema revision
```
python manage.py -r <revision>
```
* upgrade database to current revision
```
python manage.py -u
```
* add sample data to database
```
python manage.py -a
```

## Package Reference
* [fastapi](https://fastapi.tiangolo.com) FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+ based on standard Python type hints.
* [pydantic](https://pydantic-docs.helpmanual.io) Data validation and settings management using python type annotations.
* [uvicorn](https://www.uvicorn.org) Uvicorn is a lightning-fast ASGI server, built on uvloop and httptools.
* [SQLAlchemy](https://www.sqlalchemy.org) Python SQL toolkit and ORM (Object Relational Mapper)
* [SQLAlchemy Doc](https://docs.sqlalchemy.org/en/13/orm/session_basics.html) SQLAlchemy Documentation
* [Swagger UI](http://swagger.io/) Simplify API development with the Swagger open source and professional toolset.
* [OpenAPI Specification](https://swagger.io/docs/specification/basic-structure/) OpenAPI (formerly Swagger) Specification is an API description format for REST APIs
* [ReDoc](https://github.com/Redocly/redoc) The best OpenAPI documentation tool
* [requests](https://requests.readthedocs.io/en/master/) Requests is an elegant and simple HTTP library for Python.
* [python-dotenv](https://github.com/theskumar/python-dotenv) Reads the key-value pair from .env file and adds them to environment variable. 
* [python-dateutil](https://dateutil.readthedocs.io/en/stable/) powerful extensions to datetime
* [python-multipart](https://andrew-d.github.io/python-multipart/) is a streaming multipart parser for Python.
* [aiofiles](https://github.com/Tinche/aiofiles) aiofiles is an Apache2 licensed library, written in Python, for handling local disk files in asyncio applications.
* [openpyxl](https://openpyxl.readthedocs.io/) openpyxl is a Python library to read/write Excel 2010 xlsx/xlsm/xltx/xltm files.
* [dateparser](https://github.com/scrapinghub/dateparser) Generic parsing of dates in over 200 language locales plus numerous formats in a language agnostic fashion.